﻿//#define INTEGRATION_TEST

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using uFMOD;

namespace ufmodcs
{
	public class Program
	{
		[DllImport("user32.dll")]
		private static extern short GetAsyncKeyState(int vKey);

		private const int VK_RETURN = 0x0d;

		public static void Main(string[] args)
		{
			#if INTEGRATION_TEST
				PowTest();
				IntegrationTest();
				return;
			#endif

			var m = new uFMOD.uFMOD();

			//m.uFMOD_PlaySong(Encoding.ASCII.GetBytes("pacman_dreams.xm"), 0, uFMOD.uFMOD.XM_FILE);

			var song = File.ReadAllBytes("pacman_dreams.xm");
			m.uFMOD_PlaySong(song, 0, uFMOD.uFMOD.XM_MEMORY);

			Console.WriteLine("Press Enter to continue...");
			while (GetAsyncKeyState(VK_RETURN)==0) Thread.Sleep(100);
			m.uFMOD_StopSong();
		}

		private static void IntegrationTest()
		{
			string [] tunes =
			{
				"1.XM",
				"84556-blastoff.xm",
				"banana_boat_ii.xm",
				"beeper_-_epidemic.xm",
				"beepsampler.xm",
				"beep_beep.xm",
				"crazy_dreams.xm",
				"DEADLOCK.XM",
				//"deliflat.xm",
				"distant3.xm",
				"dpl_fast.xm",
				"grip_-_squarewave_stalker.xm",
				"heatwave.xm",
				"ice_dancer_20th.xm",
				"lhs_abr.xm",
				"lmt_dr_3.xm",
				"m.xm",
				"madness_final.xm",
				"megadreams.xm",
				"ml_siege.xm",
				"ni-piece.xm",
				"oler.xm",
				"pacman.xm",
				"pacman_dreams.xm",
				"pacmawax.xm",
				"pies.xm",
				"psirius_-_dawning_in_orion.xm",
				"purplesky_-_solar_winds.xm",
				"sawdance.xm",
				"squarewa.xm",
				"square_sounds_-_awe.xm",
				"surfonasinewave.xm",
				"transit.xm",
				//"vsn-beep.xm",
			};

			foreach (var tune in tunes)
			{
				var m = new uFMOD.uFMOD();

				try
				{
					var song = Encoding.ASCII.GetBytes((Path.Combine("../../../../../tests/tunes", tune)));
					m.uFMOD_PlaySong(song, 0, uFMOD.uFMOD.XM_FILE | uFMOD.uFMOD.XM_INTEGRATION_TEST);
				}
				catch (Exception ex)
				{
					Trace.WriteLine($"Exception!\n{ex}");
				}
				finally
				{
					m.uFMOD_StopSong();
				}
			}
		}

		private static void PowTest()
		{
			const float f0_0833 = 8.3333336e-2f;
			const float f13_375 = 1.3375e1f;
			const float f0_0013 = 1.302083375e-3f;
			const float f8363_0 = 8.3630004275e3f;

			var fail = new List<string>();

			for (int t = -4610; t < 4610; t++)
			{
				int tmp = t;
				double tmpf;
				tmpf = f8363_0 * Math.Pow(2.0, tmp * f0_0013);
				tmp = (int)Math.Round(f8363_0 * Math.Pow(2.0, tmp * f0_0013), MidpointRounding.ToEven);

				double e = t * f0_0013;
				double w = Math.Truncate(e);
				double f = e - w;
				double q;
				if (w>=0)
					q = Math.Pow(2.0, f) * (1 << (int)w) * f8363_0;
				else
					q = Math.Pow(2.0, f) * 1.0/(1 << -(int)w) * f8363_0;

				if (FreqTab.pow2_table_DoFlags(t) != tmp)
					fail.Add($"{t}: {FreqTab.pow2_table_DoFlags(t)} {tmp} {tmpf} {q} {Math.Round(q,0,MidpointRounding.ToEven)}");
			}

			if (fail.Count == 0)
			{
				Trace.WriteLine("[ OK ] PowTest");
			}
			else
			{
				Trace.WriteLine("[FAIL] PowTest");
				Trace.WriteLine(string.Join(Environment.NewLine, fail));
			}
		}
	}
}