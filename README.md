# ufmodcs

### An XM module player in C# 

This is a port of ufmod 1.25.2a Windows 32bit assembler to C#.

The homepage for the original code is at https://ufmod.sourceforge.io/Win32/en.htm
uFMOD sources, binaries and utility programs © 2005 - 2008 Asterix and Quantum. All rights reserved.

The license for this C# port is Public Domain.
