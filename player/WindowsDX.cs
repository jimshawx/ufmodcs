using System;
using System.Linq;
using System.Runtime.InteropServices;
using SharpDX;
using SharpDX.Multimedia;
using SharpDX.XAudio2;

namespace uFMOD
{
	public interface IAudioPlayer :IDisposable
	{
		void StartBackgroundPlay(Func<Memory<ushort>> mixer);
		void StopBackgroundPlay();
		int SetVolume(int Volume);
	}
	public class AHXXAudio2 : IAudioPlayer
	{
		private volatile int StopPlaying;
		//private int NrBlocks, Frames, BlockLen;

		[Flags]
		private enum XAUDIO2_LOG
		{
			ERRORS = 0x0001, // For handled errors with serious effects.
			WARNINGS = 0x0002, // For handled errors that may be recoverable.
			INFO = 0x0004, // Informational chit-chat (e.g. state changes).
			DETAIL = 0x0008, // More detailed chit-chat.
			API_CALLS = 0x0010, // Public API function entries and exits.
			FUNC_CALLS = 0x0020, // Internal function entries and exits.
			TIMING = 0x0040, // Delays detected and other timing data.
			LOCKS = 0x0080, // Usage of critical sections and mutexes.
			MEMORY = 0x0100, // Memory heap usage information.
			STREAMING = 0x1000, // Audio streaming information.
		}

		private readonly XAudio2 xaudio;
		private readonly MasteringVoice masteringVoice;

		private SourceVoice xaudioVoice;

		private class Block
		{
			public AudioBuffer xaudioBuffer;
			public bool Active;
		}

		private Block[] blocks;

		private const int Frequency = 48000;
		private const int Bits = 16;
		private const int NrBlocks = 16;

		public AHXXAudio2()
		{
			xaudio = new XAudio2();

			xaudio.SetDebugConfiguration(new DebugConfiguration
			{
				TraceMask = (int)(XAUDIO2_LOG.ERRORS | XAUDIO2_LOG.WARNINGS | XAUDIO2_LOG.DETAIL | XAUDIO2_LOG.API_CALLS | XAUDIO2_LOG.FUNC_CALLS),
				BreakMask = 0,
				LogThreadID = true,
				LogFileline = true,
				LogFunctionName = true,
				LogTiming = true
			}, IntPtr.Zero);

			masteringVoice = new MasteringVoice(xaudio);
			masteringVoice.GetVoiceDetails(out VoiceDetails _);

			var wv = new WaveFormat(Frequency, Bits, 2);
			
			xaudioVoice = new SourceVoice(xaudio, wv, VoiceFlags.None);

			//this.Frames = Frames;
			//this.NrBlocks = NrBlocks;

			int BlockLen = Frequency * Bits / 8 / NrBlocks;
			//48000 * 16 / 8 / 16 = 6000
			BlockLen = 0x800*2;// = 4096

			blocks = new Block[NrBlocks];
			for (int i = 0; i < NrBlocks; i++)
			{
				blocks[i] = new Block
				{
					xaudioBuffer = new AudioBuffer {AudioBytes = BlockLen, AudioDataPointer = Utilities.AllocateMemory(BlockLen), PlayLength = 0, Context = (IntPtr)i}
				};
			}

			xaudioVoice.BufferEnd += ptr => { blocks[ptr.ToInt32()].Active = false; PlayIt(); };

			StopPlaying = 0;
		}

		public void Dispose()
		{
			for (int i = 0; i < NrBlocks; i++)
				Utilities.FreeMemory(blocks[i].xaudioBuffer.AudioDataPointer);

			xaudioVoice.Stop();
			xaudioVoice.Dispose();

			masteringVoice.DestroyVoice();
			masteringVoice.Dispose();
			xaudio.Dispose();
		}

		private Func<Memory<ushort>> Mixer;
		public void StartBackgroundPlay(Func<Memory<ushort>> mixer)
		{
			Mixer = mixer;

			//prime the playback buffer(s)
			for (int i = 0; i < NrBlocks; i++)
				PlayIt();

			//start the audio
			xaudioVoice.Start();
		}

		public void StopBackgroundPlay()
		{
			StopPlaying = 1;
			//playback buffers will drain naturally
		}

		public int SetVolume(int Volume)
		{
			float v = Volume / 64.0f;
			masteringVoice.SetChannelVolumes(2, new [] {v,v});
			return 1;
		}

		private void PlayIt()
		{
			if (StopPlaying != 0) return;

			var x = blocks.First(z => !z.Active);
			x.Active = true;

			//var buff = Mixer().ToArray();
			//Marshal.Copy((short[])(object)buff, 0, x.xaudioBuffer.AudioDataPointer, buff.Length);
			
			//avoid the double mem copy caused by ToArray followed by Copy
			var buff = Mixer().Span;
			int i = 0;
			foreach (ushort b in buff)
				Marshal.WriteInt16(x.xaudioBuffer.AudioDataPointer, (i++) << 1, (short)b);

			xaudioVoice.SubmitSourceBuffer(x.xaudioBuffer, null);
		}
	}
}