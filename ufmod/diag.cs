using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;

namespace uFMOD
{
	public enum dumptype
	{
		DUMPT_CONSOLE,
		DUMPT_FILE,
	}
	
	public static class Dump
	{
		[StructLayout(LayoutKind.Sequential, Pack = 2)]
		private struct PCMWAVEFORMAT {
			public ushort    wFormatTag;        /* format type */
			public ushort    nChannels;         /* number of channels (i.e. mono, stereo, etc.) */
			public uint   nSamplesPerSec;    /* sample rate */
			public uint   nAvgBytesPerSec;   /* for buffer estimation */
			public ushort    nBlockAlign;       /* block size of data */
			public ushort        wBitsPerSample;

			public PCMWAVEFORMAT(ushort wFormatTag, ushort nChannels, uint nSamplesPerSec, uint nAvgBytesPerSec, ushort nBlockAlign, ushort wBitsPerSample)
			{
				this.wFormatTag = wFormatTag;
				this.nChannels = nChannels;
				this.nSamplesPerSec = nSamplesPerSec;
				this.nAvgBytesPerSec = nAvgBytesPerSec;
				this.nBlockAlign = nBlockAlign;
				this.wBitsPerSample = wBitsPerSample;
			}
		}
		private const int WAVE_FORMAT_PCM = 1;

		private static dumptype dumpt = dumptype.DUMPT_CONSOLE;
		private static StreamWriter df = null;

		public static void dumpset(dumptype t)
		{
			if (df!=null)
			{
				df.Close();
				df.Dispose();
				df = null;
			}

			dumpt = t;
		}

		static int ensurefile_i = 1;
		private static void ensurefile()
		{
			if (df!=null) return;

			//time_t t = time(null);
			string tmp;
			//struct tm lt;
			//localtime_s(&lt, &t);
			//sprintf_s(tmp, 1024, "dump-%04d-%02d-%02dT%02d_%02d_%02d_%d.txt", lt.tm_year + 1900, lt.tm_mon + 1, lt.tm_mday, lt.tm_hour, lt.tm_min, lt.tm_sec, i);
			tmp = $"dump-{ensurefile_i:D5}.txt";
			df = new StreamWriter(File.OpenWrite(tmp), Encoding.ASCII);
			Debug.Assert(df != null);

			string tmp2 = $"fc ufmodc\\dump-{ensurefile_i:D5}.txt ufmod\\{tmp}\n";
			Trace.Write(tmp2);

			ensurefile_i++;
		}

		private static int ind = 0;
		private static string indents = "\t\t\t\t\t\t\t\t\t\t";

		private static void indent()
		{
			if (ind < 9)
				ind++;
		}

		private static void outdent()
		{
			if (ind > 0)
				ind--;
		}

		private static readonly Regex parmRegex = new Regex(@"(?<!%)(?:%%)*%([-+0 #])?(\d+|\*)?(\.\*|\.\d+)?([hLIw]|l{1,2}|I32|I64)?([cCdiouxXeEfgGaAnpsSZ])", RegexOptions.Compiled);

		private static void dump(string fmt, params object[] p)
		{
			int c = 0;
			string repl = parmRegex.Replace(fmt, m => $"{{{c++}}}");
			string tmp = string.Format(repl, p);

			if (dumpt == dumptype.DUMPT_CONSOLE)
			{
				Trace.Write(indents.Substring(10 - ind));
				Trace.WriteLine(tmp);
			}
			else if (dumpt == dumptype.DUMPT_FILE)
			{
				ensurefile();
				df.Write(indents.Substring(10 - ind));
				df.WriteLine(tmp);
			}
		}

		static void dump_samp(int i, int s, uF_SAMP samp)
		{
			dump("sample %d", i);

			indent();

			dump("%u %s", samp._length, "sample length");
			dump("%u %s", samp.loopstart, "loop start");
			dump("%u %s", samp.looplen, "loop length");
			dump("%u %s", (uint)samp.defvol, "default volume");
			dump("%d %s", (int)samp.finetune, "finetune value from - 128 to 127");
			dump("%02x %s", (uint)samp.bytes, "type[b 0 - 1] : 0 - no loop 1 - forward loop 2 - bidirectional loop(aka ping - pong) [b 4] : 0 - 8 - bit sample data 1 - 16 - bit sample data");
			dump("%u %s", (uint)samp.defpan, "default pan value from 0 to 255");
			dump("%d %s", (int)samp.relative, "relative note(signed value)");
			dump("%u %s", (uint)samp.Resved, "reserved, samp.known values : 00h - regular delta packed sample data, ADh - ModPlug 4 - bit ADPCM packed sample data");
			dump("%u %s", (uint)samp.loopmode, "loopmode");
			//dump("%u %s", (uint)samp._align, "align");

			/*
			dump("sample data");
			indent();
			unsigned short *data = &samp.buff;
			uint len = samp._length/32;
			char tmp[512];
			int s, k=32;
			for (int j = 0; j < 2 && k; j++)
			{
				while (len--)
				{
					s = k;
					char *t = tmp;
					while (s--)
						t += sprintf_s(t, 6, "%04X ", (uint)*data++);
					dump(tmp);
				}
				len = 1;
				k = samp._length % 32;
			}
			outdent();
			*/

			{
				const int FSOUND_MixRate = 48000;
				var pcm = new PCMWAVEFORMAT
				(
					WAVE_FORMAT_PCM,
					2, //channels
					FSOUND_MixRate,
					FSOUND_MixRate * 4,
					(2 * 16) / 8, //channels * bps / 8
					16 //bps
				);

				int length;

				var f = new BinaryWriter(File.OpenWrite($"sample_{i}_{s}.wav"));
				if (f != null)
				{
					f.Write(Encoding.ASCII.GetBytes("RIFF"), 0,4);
					length = (int)(samp._length * 2 + 8 + Marshal.SizeOf(pcm) + 8);
					f.Write(length);
					f.Write(Encoding.ASCII.GetBytes("WAVE"), 0,4);

					f.Write(Encoding.ASCII.GetBytes("fmt "), 0,4);
					length = Marshal.SizeOf(pcm);
					f.Write(length);
					//f.Write(pcm, Marshal.SizeOf(pcm), 1);
					f.Write(pcm.wFormatTag);
					f.Write(pcm.nChannels);
					f.Write(pcm.nSamplesPerSec);
					f.Write(pcm.nAvgBytesPerSec);
					f.Write(pcm.nBlockAlign);
					f.Write(pcm.wBitsPerSample);

					length = (int)(samp._length * 2);
					f.Write(Encoding.ASCII.GetBytes("data"), 0,4);
					f.Write(length);
					var sampBytes = samp.buff.Select(x => new[] {(byte)x, (byte)(x >> 8)}).SelectMany(x => x).ToArray();
					f.Write(sampBytes, 0, sampBytes.Length);

					f.Close();
					f.Dispose();
				}
			}

			outdent();
		}

		static void dump_inst(int i, uF_INST inst)
		{
			//char tmp[1024], *t;
			var tmp = new StringBuilder();

			dump("instrument %d", i);

			indent();

			dump("keymap - sample keymap assignments");
			indent();
			tmp.Clear();
			for (int j = 0; j < 48; j++)
				tmp.Append($"{inst.keymap[j]:X2} ");
			dump("%s", tmp.ToString());
			tmp.Clear();
			for (int j = 48; j < 96; j++)
				tmp.Append($"{inst.keymap[j]:X2} ");
			dump("%s", tmp.ToString());
			outdent();

			dump("VOLPoints - volume envelope points");
			indent();
			tmp.Clear();
			for (int j = 0; j < 24; j++)
				tmp.Append($"{inst.VOLPoints[j]:X4} ");
			dump("%s", tmp);
			outdent();

			dump("PANPoints - panning envelope points");
			indent();
			tmp.Clear();
			for (int j = 0; j < 24; j++)
				tmp.Append($"{inst.PANPoints[j]:X4} ");
			dump("%s", tmp);
			outdent();

			dump("%u %s", (uint)inst.VOLnumpoints, "number of volume envelope points");
			dump("%u %s", (uint)inst.PANnumpoints, "number of panning envelope points");
			dump("%u %s", (uint)inst.VOLsustain, "volume sustain point");
			dump("%u %s", (uint)inst.VOLLoopStart, "volume envelope loop start");
			dump("%u %s", (uint)inst.VOLLoopEnd, "volume envelope loop end");
			dump("%u %s", (uint)inst.PANsustain, "panning sustain point");
			dump("%u %s", (uint)inst.PANLoopStart, "panning envelope loop start");
			dump("%u %s", (uint)inst.PANLoopEnd, "panning envelope loop end");
			dump("%u %s", (uint)inst.VOLtype, "type of envelope, bit 0:On 1:Sustain 2:Loop");
			dump("%u %s", (uint)inst.PANtype, "type of envelope, bit 0:On 1:Sustain 2:Loop");
			dump("%u %s", (uint)inst.VIBtype, "instrument vibrato type");
			dump("%u %s", (uint)inst.VIBsweep, "time it takes for vibrato to fully kick in");
			dump("%u %s", (uint)inst.iVIBdepth, "depth of vibrato");
			dump("%u %s", (uint)inst.VIBrate, "rate of vibrato");
			dump("%u %s", (uint)inst.VOLfade, "fade out value");

			for (int s = 0; s < 16 && inst.sample[s] != null; s++)
				dump_samp(i, s, inst.sample[s]);

			outdent();
		}

		public static void dumpmodule(uF_MOD_STATE _mod)
		{
			uF_MOD mod = _mod.module;
			uF_INST[] inst = mod.instrument;

			for (int i = 0; i < mod.numinsts; i++)
				dump_inst(i, inst[i]);

			dump("orders");
			indent();
			for (int i = 0; i < mod.numorders; i++)
				dump("%u %s", (uint)
			mod.orderlist[i], "pattern playing order list");
			outdent();
			for (int i = 0; i < mod.numpatternsmem; i++)
				dumppat(i, (int)mod.numchannels, mod.pattern[i]);
		}

		private static void dumpfchan(int i, FSOUND_CHANNEL chan)
		{
			dump("channel (FSOUND_CHANNEL) %d", i);
			indent();

			dump("%u %s", chan.actualvolume, "volume");
			dump("%u %s", chan.actualpan, "panning value");
			dump("%u %s", chan.fsampleoffset, "sample offset(sample starts playing from here)");
			dump("%u %s", chan.leftvolume, "adjusted volume for left channel(panning involved)");
			dump("%u %s", chan.rightvolume, "adjusted volume for right channel(panning involved)");
			dump("%u %s", chan.mixpos, "high part of 32:32 fractional position in sample");
			dump("%u %s", chan.speedlo, "playback rate - low part fractional");
			dump("%d %s", chan.speedhi, "playback rate - high part fractional");
			dump("%u %s", chan.ramp_LR_target, "ramp_LR_target");
			dump("%d %s", chan.ramp_leftspeed, "ramp_leftspeed");
			dump("%d %s", chan.ramp_rightspeed, "ramp_rightspeed");
			//dump("%p %s", chan.fsptr, "pointer to FSOUND_SAMPLE currently playing sample");
			dump("%u %s", chan.mixposlo, "low part of 32:32 fractional position in sample");
			dump("%u %s", chan.ramp_leftvolume, "ramp_leftvolume");
			dump("%u %s", chan.ramp_rightvolume, "ramp_rightvolume");
			dump("%u %s", (uint)chan.ramp_count, "ramp_count");
			dump("%u %s", (uint)chan.speeddir, "playback direction - 0 forwards or 1 backwards");

			outdent();
		}

		private static void dumpchan(uF_MOD module, int i, uF_CHAN chan)
		{
			dump("channel (uF_CHAN) %d", i);
			indent();

			dump("%u %s", (uint)chan.note, "last note set in channel");
			dump("%u %s", (uint)chan.samp, "last sample set in channel");
			dump("%u %s", (uint)chan.notectrl, "flags for DoFlags proc");
			dump("%u %s", (uint)chan.inst, "last instrument set in channel");
			//dump("%p %s", chan.cptr, "pointer to FSOUND_CHANNEL system mixing channel");
			//dump("%d %s", chan.cptr - module.Channels, "number of FSOUND_CHANNEL system mixing channel");
			dump("%u %s", chan.freq, "current mod frequency period for this channel");
			dump("%d %s", chan.volume, "current mod volume for this channel");
			dump("%d %s", chan.voldelta, "delta for volume commands : tremolo / tremor, etc");
			dump("%d %s", chan.freqdelta, "delta for frequency commands : vibrato / arpeggio, etc");
			dump("%u %s", chan.pan, "current mod pan for this channel");

			dump("%u %s", chan.envvol.tick, "tick counter for envelope position volume");
			dump("%u %s", chan.envvol.pos, "envelope position volume");
			dump("%u %s", chan.envvol.delta, "delta step between points volume");

			dump("%u %s", chan.envpan.tick, "tick counter for envelope position pan");
			dump("%u %s", chan.envpan.pos, "envelope position pan");
			dump("%u %s", chan.envpan.delta, "delta step between points pan");

			dump("%u %s", chan.ivibsweeppos, "instrument vibrato sweep position");
			dump("%u %s", chan.ivibpos, "instrument vibrato position");
			dump("%u %s", (uint)chan.keyoff, "flag whether keyoff has been hit or not");
			dump("%u %s", (uint)chan.envvolstopped, "flag to say whether envelope has finished or not");
			dump("%u %s", (uint)chan.envpanstopped, "flag to say whether envelope has finished or not");

			dump("%d %s", chan.envvolfrac.frac, "fractional interpolated envelope volume");
			dump("%u %s", chan.envvolfrac.val, "final interpolated envelope volume");

			dump("%d %s", chan.fadeoutvol, "volume fade out");

			dump("%d %s", chan.envpanfrac.frac, "fractional interpolated envelope pan");
			dump("%u %s", chan.envpanfrac.val, "final interpolated envelope pan");

			dump("%u %s", chan.period, "last period set in channel");
			dump("%u %s", chan.sampleoffset, "sample offset for this channel in SAMPLES");
			dump("%u %s", chan.portatarget, "note to porta to");
			dump("%u %s", chan.patloopno, "pattern loop variables for effect E6x");
			dump("%u %s", chan.patlooprow, "patlooprow");
			dump("%u %s", (uint)chan.realnote, "last realnote set in channel");
			dump("%u %s", (uint)chan.recenteffect, "previous row's effect. used to correct tremolo volume");
			dump("%d %s", (int)chan.portaupdown, "last porta up / down value");
			dump("%u %s", (uint)chan.unused, "unused");
			dump("%u %s", (uint)chan.xtraportadown, "last porta down value");
			dump("%u %s", (uint)chan.xtraportaup, "last porta up value");
			dump("%u %s", (uint)chan.volslide, "last volume slide value");
			dump("%u %s", (uint)chan.panslide, "pan slide parameter");
			dump("%u %s", (uint)chan.retrigx, "last retrig volume slide used");
			dump("%u %s", (uint)chan.retrigy, "last retrig tick count used");
			dump("%u %s", (uint)chan.portaspeed, "porta speed");
			dump("%u %s", (uint)chan.vibpos, "vibrato position");
			dump("%u %s", (uint)chan.vibspeed, "vibrato speed");
			dump("%d %s", (int)chan.vibdepth, "vibrato depth");
			dump("%u %s", (uint)chan.tremolopos, "tremolo position");
			dump("%u %s", (uint)chan.tremolospeed, "tremolo speed");
			dump("%d %s", (int)chan.tremolodepth, "tremolo depth");
			dump("%u %s", (uint)chan.tremorpos, "tremor position");
			dump("%u %s", (uint)chan.tremoron, "remembered parameters for tremor");
			dump("%u %s", (uint)chan.tremoroff, "remembered parameters for tremor");
			dump("%u %s", (uint)chan.wavecontrol, "waveform type for vibrato and tremolo (4bits each)");
			dump("%d %s", (int)chan.finevslup, "parameter for fine volume slide down");
			dump("%u %s", (uint)chan.fineportaup, "parameter for fine porta slide up");
			dump("%u %s", (uint)chan.fineportadown, "parameter for fine porta slide down");

			outdent();
		}

		private static void dumpnote(int i, uF_NOTE note)
		{
			dump("%u %s", (uint)note.unote, "note to play at(0 - 97) 97 = keyoff");
			dump("%u %s", (uint)note.number, "sample being played(0 - 128)");
			dump("%u %s", (uint)note.uvolume, "volume column value(0 - 64)  255 = no volume");
			dump("%u %s", (uint)note.effect, "effect number(0 - 1Ah)");
			dump("%u %s", (uint)note.eparam, "effect parameter(0 - 255)");
		}

		private static void dumppat(int i, int num_channels, uF_PAT pat)
		{
			dump("pattern %d", i);
			indent();

			dump("%d %s", (int)pat.rows, "rows");
			dump("%u %s", (uint)pat.patternsize, "patternsize");

			dump("notes");
			indent();
			//for (int j = 0; j < num_channels * pat.rows; j++)
			//	dumpnote(j, &pat.data[j]);

			var t = new StringBuilder[5];
			uF_NOTE note;
			int notei = 0;

			if (pat.data == null)
			{
				dump("Pattern data is null!");
				outdent();
				outdent();
				return;
			}

			for (int r = 0; r < pat.rows; r++)
			{
				for (int f = 0; f < 5; f++)
					t[f] = new StringBuilder();
				uint cnt = 0;
				for (int c = 0; c < num_channels; c++)
				{
					note = pat.data[notei];
					t[0].Append($"{(uint)note.unote,3} ");
					t[1].Append($"{(uint)note.number,3} ");
					t[2].Append($"{(uint)note.uvolume,3} ");
					t[3].Append($" {(uint)note.effect:X2} ");
					t[4].Append($"{(uint)note.eparam,3} ");
					cnt += (uint)note.unote + note.number + note.uvolume + note.effect + note.eparam;
					notei++;
				}

				if (cnt != 0)
				{
					t[0].Append("unote");
					t[1].Append("number");
					t[2].Append("uvolume");
					t[3].Append("effect");
					t[4].Append("eparam\n");
					for (int f = 0; f < 5; f++)
						dump("%s", t[f].ToString());
				}
				else
				{
					dump("empty row");
				}
			}

			outdent();

			outdent();
		}

		private static void dumpufmod(uF_MOD mod)
		{
			//dump("%p %s", mod.pattern, "pointer to FMUSIC_PATTERN array for this song");
			//dump("%p %s", mod.instrument, "pointer to FMUSIC_INSTRUMENT array for this song");

			dump("%u %s", mod.mixer_samplesleft, "");
			;
			dump("%u %s", mod.globalvolume, "global mod volume");
			dump("%u %s", mod.tick, "current mod tick");
			dump("%u %s", mod.speed, "speed of song in ticks per row");
			dump("%d %s", mod.order, "current song order position");
			dump("%d %s", mod.row, "current row in pattern");
			dump("%u %s", mod.patterndelay, "pattern delay counter");
			dump("%d %s", mod.nextorder, "current song order position");
			dump("%d %s", mod.nextrow, "current row in pattern");
			//dump("%u %s", mod.unused1, "unused1");
			dump("%u %s", mod.numchannels, "number of channels");
			//dump("%p %s", mod.Channels, "channel pool");
			//dump("%p %s", mod.uFMOD_Ch, "channel array for this song");

			dump("%u %s", mod.mixer_samplespertick, "mixer_samplespertick");
			dump("%u %s", mod.header_size, "header_size");

			dump("%d %s", (int)mod.numorders, "number of orders(song length)");
			dump("%u %s", (uint)mod.restart, "restart position");
			dump("%u %s", (uint)mod.numchannels_xm, "numchannels_xm");
			dump("%u %s", (uint)mod.globalvsl, "global mod volume // numchannels_xm is usually a word, this has been jammed in between since numchannels is <= 32");
			dump("%u %s", (uint)mod.numpatternsmem, "number of allocated patterns");
			dump("%u %s", (uint)mod.numinsts, "number of instruments");
			dump("%u %s", (uint)mod.flags, "flags such as linear frequency, format specific quirks, etc");
			dump("%u %s", (uint)mod.defaultspeed, "defaultspeed");
			dump("%u %s", (uint)mod.defaultbpm, "defaultbpm");

		}

		public static void dumpmod(uF_MOD_STATE _mod)
		{
			dumpufmod(_mod.module);

			//for (int i = 0; i < 3; i++)
			//	dump("%u %s", _mod.mmt[i], "mmt");

			//dump("%p %s", _mod.hHeap, "Heap");
			dump("%p %s", _mod.hThread, "Thread");
			//dump("%p %s", _mod.hWaveOut, "hWaveOut");
			dump("%u %s", _mod.uFMOD_FillBlk, "uFMOD_FillBlk");
			dump("%p %s", _mod.SW_Exit, "file ptr");

			//for (int i = 0; i < FSOUND_BlockSize*2; i++)
			//	dump("%u", _mod.MixBuf[i]);

			dump("%u %s", (uint)_mod.ufmod_noloop, "enable/disable restart loop");
			dump("%u %s", (uint)_mod.ufmod_pause, "pause/resume");
			for (int i = 0; i < 2; i++)
				dump("%u %s", (uint)
			_mod.mix_endflag[i], "mix_endflag");

			dump("%u %s", _mod.mmf_module_size, "module size");
			dump("%u %s", _mod.mmf_module_posn, "module position");
			dump("%p %s", _mod.mmf_module_address, "module address/resource");
			//dump("%u %s", _mod.mmf_pad, "mmf pad");

			dump("%u %s", _mod.ufmod_vol, "global volume scale");

			dump("%p %s", _mod.uFMOD_fopen, "uFMOD_fopen");
			dump("%p %s", _mod.uFMOD_fread, "uFMOD_fread");
			dump("%p %s", _mod.uFMOD_fclose, "uFMOD_fclose");
			dump("%u %s", _mod.unused, "unused");

			//for (int i = 0; i < FSOUND_BufferSize; i++)
			//	dump("%u", _mod.databuf[i];

			//dump("%p %s", _mod.MixBlock.lpData, "pointer to locked data buffer");
			//dump("%u %s", _mod.MixBlock.dwBufferLength, "length of data buffer");
			//dump("%u %s", _mod.MixBlock.dwBytesRecorded, "used for input only");
			//dump("%p %s", _mod.MixBlock.dwUser, "for client's use");
			//dump("%u %s", _mod.MixBlock.dwFlags, "assorted flags (see defines)");
			//dump("%u %s", _mod.MixBlock.dwLoops, "loop control counter");
			//dump("%p %s", _mod.MixBlock.lpNext, "reserved for driver");
			//dump("%p %s", _mod.MixBlock.reserved, "reserved for driver");

			for (int i = 0; i < 4; i++)
				dump("%u %s", (uint)
			_mod.RealBlock[i], "RealBlock");

			dump("%u %s", _mod.time_ms, "time_ms");

			//for (int i = 0; i < totalblocks; i++)
			//	dump("%u %s", _mod.uF_STATS tInfo[i];

			dump("%s %s", _mod.szTtl, "track title");

			//dump("%u %s", _mod.uF_SAMP DummySamp;
		}

		public static void dumpstate(string when, uF_MOD_STATE _mod)
		{
			dump("%s", when);
			indent();

			dumpufmod(_mod.module);

			uF_MOD mod = _mod.module;
			FSOUND_CHANNEL[] fchan = mod.Channels;
			uF_CHAN[] chan = mod.uFMOD_Ch;

			for (int i = 0; i < (int)mod.numchannels * 2; i++)
				dumpfchan(i, fchan[i]);

			for (int i = 0; i < (int)mod.numchannels; i++)
				dumpchan(_mod.module, i, chan[i]);

			outdent();
		}

		private static void dumpb(byte[] data, uint count)
		{
			const int perrow = 32;
			uint len = count / perrow;
			var tmp = new StringBuilder();
			int s, k = perrow;
			int datai = 0;
			for (int j = 0; j < 2 && k!=0; j++)
			{
				while (len-- != 0)
				{
					s = k;
					tmp.Clear();
					while (s--!=0)
						tmp.Append($"{data[datai++]:X2} ");
					dump(tmp.ToString());
				}

				len = 1;
				k = (int)(count % perrow);
			}
		}

		static void dumpw(ushort[] data, uint count)
		{
			const int perrow = 32;
			uint len = count / perrow;
			var tmp = new StringBuilder();
			int s, k = perrow;
			int datai = 0;
			for (int j = 0; j < 2 && k!=0; j++)
			{
				while (len--!=0)
				{
					s = k;
					tmp.Clear();
					while (s--!=0)
						tmp.Append($"{data[datai++]:X4} ");
					dump(tmp.ToString());
				}

				len = 1;
				k = (int)(count % perrow);
			}
		}

		private static void dumpl(uint[] data, uint count)
		{
			const int perrow = 16;
			uint len = count / perrow;
			var tmp = new StringBuilder();
			int s, k = perrow;
			int datai = 0;
			for (int j = 0; j < 2 && k!=0; j++)
			{
				while (len--!=0)
				{
					s = k;
					tmp.Clear();
					while (s--!=0)
						tmp.Append($"{data[datai++]:X8} ");
					dump(tmp.ToString());
				}

				len = 1;
				k = (int)(count % perrow);
			}
		}

		public static void dumpbuffer(string name, Array ptr, uint count, byte bits)
		{
			dump("%s", name);
			indent();
			switch (bits)
			{
				case 8:
					dumpb((byte[])ptr, count);
					break;
				case 16:
					dumpw((ushort [])ptr, count);
					break;
				case 32:
					dumpl((uint [])ptr, count);
					break;
			}

			outdent();
		}

	}
}