﻿using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace uFMOD
{
	public static class Test
	{
		[StructLayout(LayoutKind.Sequential, Pack = 2)]
		private struct PCMWAVEFORMAT {
			public ushort    wFormatTag;        /* format type */
			public ushort    nChannels;         /* number of channels (i.e. mono, stereo, etc.) */
			public uint   nSamplesPerSec;    /* sample rate */
			public uint   nAvgBytesPerSec;   /* for buffer estimation */
			public ushort    nBlockAlign;       /* block size of data */
			public ushort        wBitsPerSample;

			public PCMWAVEFORMAT(ushort wFormatTag, ushort nChannels, uint nSamplesPerSec, uint nAvgBytesPerSec, ushort nBlockAlign, ushort wBitsPerSample)
			{
				this.wFormatTag = wFormatTag;
				this.nChannels = nChannels;
				this.nSamplesPerSec = nSamplesPerSec;
				this.nAvgBytesPerSec = nAvgBytesPerSec;
				this.nBlockAlign = nBlockAlign;
				this.wBitsPerSample = wBitsPerSample;
			}
		}

		private static BinaryWriter f;
		private static Stream s;

		private const int WAVE_FORMAT_PCM = 1;
		public static void integration_begin(string fname)
		{
			const int FSOUND_MixRate = 48000;
			var pcm = new PCMWAVEFORMAT
			(
				WAVE_FORMAT_PCM,
				2, //channels
				FSOUND_MixRate,
				FSOUND_MixRate * 4,
				(2 * 16) / 8, //channels * bps / 8
				16 //bps
			);

			int length;

			s = File.OpenWrite(fname.Replace("/tunes", "/evaluate")+".wav");
			f = new BinaryWriter(s);

			if (f != null)
			{
				f.Write(Encoding.ASCII.GetBytes("RIFF"), 0,4);
				length = -1;
				f.Write(length);
				f.Write(Encoding.ASCII.GetBytes("WAVE"), 0,4);

				f.Write(Encoding.ASCII.GetBytes("fmt "), 0,4);
				length = Marshal.SizeOf(pcm);
				f.Write(length);
				//f.Write(pcm, Marshal.SizeOf(pcm), 1);
				f.Write(pcm.wFormatTag);
				f.Write(pcm.nChannels);
				f.Write(pcm.nSamplesPerSec);
				f.Write(pcm.nAvgBytesPerSec);
				f.Write(pcm.nBlockAlign);
				f.Write(pcm.wBitsPerSample);

				length = -1;
				f.Write(Encoding.ASCII.GetBytes("data"), 0,4);
				f.Write(length);
			}
		}

		public static void integration(ushort[] ptr)
		{
			var sampBytes = ptr.Select(x => new[] {(byte)x, (byte)(x >> 8)}).SelectMany(x => x).ToArray();
			//var sampBytes = ptr.Select(x => new[] {(byte)(x>>8), (byte)x}).SelectMany(x => x).ToArray();
			f.Write(sampBytes, 0, sampBytes.Length);
		}

		public static void integration_end()
		{
			f.Flush();
			s.Flush();
			s.Close();
			s.Dispose();
			f.Close();
			f.Dispose();
		}
	}
}