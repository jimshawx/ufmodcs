//#define USE_DUMP

using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace uFMOD
{
	public class uFMOD
	{
		private const int FSOUND_MixRate = 48000;
		private const int FREQ_40HZ_p = 0x1B4E8;
		private const int FREQ_40HZ_f = 0x369D00;

		private const int volumerampsteps = 128;
		private const int volumeramps_pow = 7;

		private const int uF_XM_size = 60;//(sizeof(uF_XM));
		private const int uF_INSTRUMENT_HEADER_size = 33;//(sizeof(uF_INSTRUMENT_HEADER));

		public const int XM_MEMORY = 1;
		public const int XM_FILE = 2;
		public const int XM_NOLOOP = 8;
		public const int XM_SUSPENDED = 16;
		public const int XM_INTEGRATION_TEST = 32;

		private const int FMUSIC_ENVELOPE_SUSTAIN = 2;
		private const int FMUSIC_ENVELOPE_LOOP = 4;
		private const int FMUSIC_FREQ = 1;
		private const int FMUSIC_VOLUME = 2;
		private const int FMUSIC_PAN = 4;
		private const int FMUSIC_TRIGGER = 8;
		private const int FMUSIC_VOLUME_OR_FREQ = 3;
		private const int FMUSIC_VOLUME_OR_PAN = 6;
		private const int FMUSIC_VOL_OR_FREQ_OR_TR = 11;
		private const int FMUSIC_FREQ_OR_TRIGGER = 9;
		private const int NOT_FMUSIC_TRIGGER = 0xF7;
		private const int NOT_FMUSIC_TRIGGER_OR_FRQ = 0xF6;

		private const int FSOUND_Block = 10;
		private const int fragmentsmask = 0xF;
		private const int FSOUND_BlockSize = (1 << FSOUND_Block);
		private const int totalblocks = (fragmentsmask + 1);
		private const int FSOUND_BufferSize = ((FSOUND_BlockSize) * totalblocks);

		private const int SAMPLE_LOOP_MASK = 3;
		private const int SAMPLE_LOOP_TYPE_NONE = 0;
		private const int SAMPLE_LOOP_TYPE_FORWARD = 1;
		private const int SAMPLE_LOOP_TYPE_PINGPONG = 3;

		private const int MODPLUG_4BIT_ADPCM = 0xAD;

		private const int WAVE_FORMAT_PCM = 1;

		private static uint rotate_right32(uint value, int count)
		{
			count &= 31;
			uint i = value >> count;
			uint j = value << (32 - count);
			return i | j;
		}

		private static uint rotate_left32(uint value, int count)
		{
			count &= 31;
			uint i = value << count;
			uint j = value >> (32 - count);
			return i | j;
		}

		private static uint shrd(uint hi, uint lo, int count)
		{
			return (hi >> count) | (lo << (32 - count));
		}

		private static uint cdq(uint value)
		{
			return (value & 0x80000000) != 0 ? 0xffffffff : 0;
		}

		//thanks to
		//https://www.musicdsp.org/en/latest/Other/222-fast-exp-approximations.html
		//ln(a^n) = n * ln(a)
		//a=2, ln(2) = 0.693...
		//2^n = e^(ln(2) * n)
		//e^(i+f) = e^i * e^f (we can take i as the whole part of n, f as the fractional part)
		//e^ln(2)*i * e^ln(2)*f
		//2^i * e^(0.693 * f)
		//(1<<i) * Taylor series of exp(x) for small x
		private float pow2f(float x)
		{
			int i = (int)x;
			x = (x - (float)i) * 0.69314718056f;//ln(2)
			if (i >= 0)
				return ((720 + x * (720 + x * (360 + x * (120 + x * (30 + x * (6 + x)))))) * 0.0013888888f) * (1 << i);
			else
				return ((720 + x * (720 + x * (360 + x * (120 + x * (30 + x * (6 + x)))))) * 0.0013888888f) * (1.0f / (1 << -i));
		}

		private static uF_MOD_STATE _mod = new uF_MOD_STATE();

		public void uFMOD_Jump2Pattern(int pat)
		{
			_mod.module.nextrow = 0;
			if (pat < _mod.module.numorders)
				_mod.module.nextorder = pat;
		}

		private static readonly ushort[] vol_scale = {
			1, // -45 dB
			130, // -24
			164, // -23
			207, // -22
			260, // -21
			328, // -20
			413, // -19
			519, // -18
			654, // -17
			823, // -16
			1036, // -15
			1305, // -14
			1642, // -13
			2068, // -12
			2603, // -11
			3277, // -10
			4125, // -9
			5193, // -8
			6538, // -7
			8231, // -6
			10362, // -5
			13045, // -4
			16423, // -3
			20675, // -2
			26029, // -1
			32768 // 0 dB
		};

		public void uFMOD_SetVolume(uint scale)
		{
			if (scale <= 25)
				_mod.ufmod_vol = vol_scale[scale];
		}

		public void uFMOD_Pause()
		{
			_mod.ufmod_pause = 1;
		}

		public void uFMOD_Resume()
		{
			_mod.ufmod_pause = 0;
		}

		public uint uFMOD_GetStats()
		{
			uint blk = _mod.RealBlock[0];
			return (uint)(_mod.tInfo[blk].R_vol | (_mod.tInfo[blk].L_vol << 16));
		}

		public uint uFMOD_GetRowOrder()
		{
			uint blk = _mod.RealBlock[0];
			return (uint)(_mod.tInfo[blk].s_row | (_mod.tInfo[blk].s_order << 16));
		}

		public uint uFMOD_GetTime()
		{
			return _mod.time_ms;
		}

		//private byte[] alloc(uint size)
		//{
		//	return new byte[size];
		//}

		private void mem_read(byte[] eax_buff, uint edx_readsize)
		{
			if (_mod.mmf_module_posn + edx_readsize >= _mod.mmf_module_size)
				edx_readsize = _mod.mmf_module_size - _mod.mmf_module_posn;

			//copy:
			if (edx_readsize > 0)
			{
				//char* src = (byte*)_mod.mmf_module_address + _mod.mmf_module_posn;
				byte[] src = _mod.mmf_module_address;
				int srci = (int)_mod.mmf_module_posn;
				int dsti = 0;
				_mod.mmf_module_posn += edx_readsize;
				//mem_do_copy:
				while (edx_readsize-- != 0)
					eax_buff[dsti++] = src[srci++];
			}
		}

		public string uFMOD_GetTitle()
		{
			return _mod.szTtl;
		}

		private void mem_open(byte[] song)
		{
			_mod.mmf_module_size = (uint)song.Length;
			_mod.mmf_module_posn = 0;
		}

		private void mem_close() { }

		//private void res_open(byte[] pszName)
		//{
		//	HANDLE hm;
		//	HRSRC hr;
		//	DWORD size;
		//	HGLOBAL res;
		//	hm = GetModuleHandle(null);//won't work in a DLL
		//	hr = FindResource(_mod.hInstance, pszName, MAKEINTRESOURCE(RT_RCDATA));
		//	assert(hr != null);
		//	size = SizeofResource(hm, hr);
		//	res = LoadResource(hm, hr);
		//	_mod.mmf_module_size = size;
		//	_mod.mmf_module_posn = 0;
		//	_mod.mmf_module_resource = res;
		//}

		private void file_open(byte[] esi_lpFileName)
		{
			//_mod.SW_Exit = File.OpenRead(Encoding.UTF8.GetString(esi_lpFileName));
			//_mod.mmf_module_size = (uint)_mod.SW_Exit.Length;
			//_mod.mmf_module_posn = 0;

			//read the entire file in and switch to memory access

			_mod.mmf_module_address = File.ReadAllBytes(Encoding.UTF8.GetString(esi_lpFileName));
			_mod.mmf_module_size = (uint)_mod.mmf_module_address.Length;
			_mod.mmf_module_posn = 0;

			_mod.uFMOD_fread = mem_read;
			_mod.uFMOD_fclose = mem_close;
		}

		private int do_read(uint ecx_offset, byte[] edi_buffer, uint esi_size)
		{
			_mod.SW_Exit.Seek(ecx_offset, SeekOrigin.Begin);
			_mod.SW_Exit.Read(edi_buffer, 0, (int)esi_size);
			return 0;
		}

		private void file_read(byte[] eax_buf, uint edx_size)
		{
			//remove all the cacheing
			do_read(_mod.mmf_module_posn, eax_buf, edx_size);

			_mod.mmf_module_posn += edx_size;
			if (_mod.mmf_module_posn > _mod.mmf_module_size)
				_mod.mmf_module_posn = _mod.mmf_module_size;
		}

		private void file_close()
		{
			_mod.SW_Exit.Close();
			_mod.SW_Exit.Dispose();
		}

		private const int UFMOD_LSEEK_SET = 0;
		private const int UFMOD_LSEEK_CUR = 1;
		private void uFMOD_lseek(int eax_pos, int ecx_org, int Z_ufmod_lseek)
		{
			if (Z_ufmod_lseek == UFMOD_LSEEK_CUR)
				eax_pos += (int)_mod.mmf_module_posn;
			if (eax_pos >= 0 && (uint)eax_pos < _mod.mmf_module_size)
				_mod.mmf_module_posn = (uint)eax_pos;
		}

		public object uFMOD_StopSong()
		{
			return uFMOD_PlaySong(null, 0, 0);
		}

		public object uFMOD_PlaySong(byte[] lpXM, uint param, int fdwSong)
		{
			uFMOD_FreeSong();

			if (lpXM==null)
				return null;

			if ((fdwSong & XM_MEMORY)!=0)
			{
				_mod.uFMOD_fopen = mem_open;
				_mod.uFMOD_fread = mem_read;
				_mod.uFMOD_fclose = mem_close;
			}
			else if ((fdwSong & XM_FILE)!=0)
			{
				_mod.uFMOD_fopen = file_open;
				_mod.uFMOD_fread = file_read;
				_mod.uFMOD_fclose = file_close;
			}
			else
			{
				return 0;
				//_mod.uFMOD_fopen = res_open;
				//_mod.uFMOD_fread = res_read;
				//_mod.uFMOD_fclose = res_close;
			}

			if ((fdwSong & XM_NOLOOP)!=0)
				_mod.ufmod_noloop = 1;

			if ((fdwSong & XM_SUSPENDED)!=0)
				_mod.ufmod_pause = XM_SUSPENDED;

			if (_mod.ufmod_vol == 0)
				_mod.ufmod_vol = 32768;

			_mod.mmf_module_address = lpXM;

			if (LoadXM(lpXM)!=0)
			{
				uFMOD_FreeSong();
				return 0;
			}

			_mod.uFMOD_fclose();

			if ((fdwSong & XM_INTEGRATION_TEST)!=0)
			{
				Test.integration_begin(Encoding.ASCII.GetString(lpXM));

				_mod.SW_Exit = null;
				_mod.ufmod_noloop = 1;
				for (;;)
				{
					uFMOD_SW_Fill();
					
					Test.integration(_mod.databuf[(int)(_mod.uFMOD_FillBlk << (FSOUND_Block + 1))..(int)((_mod.uFMOD_FillBlk << (FSOUND_Block + 1)) + FSOUND_BlockSize*2)]);

					if (_mod.SW_Exit != null)
						break;
				}
				
				Test.integration_end();
				return 0;
			}

			_mod.SW_Exit = null;

			_mod.hThread = new AHXXAudio2();
			_mod.hThread.StartBackgroundPlay(()=>
			{
				int blk = (int)_mod.uFMOD_FillBlk;
				uFMOD_SW_Fill();
				//return _mod.databuf[(blk * FSOUND_BlockSize * 2)..((blk + 1) * FSOUND_BlockSize * 2)];
				return new Memory<ushort>(_mod.databuf, blk * FSOUND_BlockSize * 2, FSOUND_BlockSize * 2);
			});

			//	return &_mod.hWaveOut;
			return new object();
		}

		public void uFMOD_FreeSong()
		{
			if (_mod.hThread != null)
			{
				_mod.SW_Exit = new MemoryStream();
				_mod.hThread.StopBackgroundPlay();
			}
			thread_finished();
		}

		private void thread_finished()
		{
			_mod.hThread = null;
			_mod.uFMOD_FillBlk = 0;

			Array.Clear(_mod.RealBlock, 0, _mod.RealBlock.Length);
			_mod.time_ms = 0;

			_mod.szTtl = null;

			foreach (var tinfo in _mod.tInfo) tinfo.Clear();

			_mod.SW_Exit = new MemoryStream();
		}

		private static int frameCount = 0;
		private void uFMOD_SW_Fill()
		{
			uF_MOD module = _mod.module;
			uint blocksizetomix;

			// MIXBUFFER CLEAR
			Array.Clear(_mod.MixBuf, 0, _mod.MixBuf.Length);

			if (_mod.ufmod_pause != 0)
				goto do_swfill;

			uint samplesleft = module.mixer_samplesleft;
			// UPDATE MUSIC
			uint[] mixbuff = _mod.MixBuf;
			int mixbuffi = 0;

			blocksizetomix = FSOUND_BlockSize;
			do
			{
				//fill_loop_1:
				if (samplesleft == 0)
				{
					int row;
					int order;
					uF_PAT[] patternbase;
					uF_PAT pattern;
					uint speed;

					// UPDATE XM EFFECTS
					patternbase = module.pattern;
					if (module.tick == 0)
					{
						order = module.nextorder;
						row = module.nextrow;
						module.nextorder = -1;
						module.nextrow = -1;

						if (order >= 0)
							module.order = order;
						//fill_nextrow:
						if (row >= 0)
							module.row = row;
						//update_note:
						
						pattern = DoNote(module, patternbase);

						if (module.nextrow != -1)
							goto inc_tick;

						row = module.row;
						row++;

						// if end of pattern
						// "if(mod.nextrow >= mod.pattern[mod.orderlist[mod.order]].rows)"
						if (row >= pattern.rows)
						{
							int order2 = module.order;
							int numorders = module.numorders;
							order2++;
							row = 0;
							if (order2 >= numorders)
							{
								if (_mod.ufmod_noloop != 0)
								{
									//thread_finished();
									uFMOD_FreeSong();
									return;
								}
								//set_restart:
								order2 = module.restart;
								if (order2 >= numorders)
									order2 = 0;
							}
							//set_nextorder:
							module.nextorder = order2;
						}

						//set_nextrow:
						module.nextrow = row;
					}
					else
					{
						DoEffs(module);
						
					}
				inc_tick:
					speed = module.speed;
					samplesleft = module.mixer_samplespertick;
					module.tick++;
					speed += module.patterndelay;
					if (module.tick >= speed)
					{
						module.patterndelay = 0;
						module.tick = 0;
					}
				}
				//mixedleft_nz:;
				uint samplestomix;
				samplestomix = blocksizetomix;
				if (samplesleft < samplestomix)
					samplestomix = samplesleft;
				//fill_ramp:;

				int mixbuffend;
				blocksizetomix -= samplestomix;
				mixbuffend = mixbuffi+ (int)(samplestomix * 2);

				Ramp(module, (int)samplestomix, mixbuff, mixbuffi, ref mixbuffend);
				mixbuffi = mixbuffend;

				{
					// time_ms += SamplesToMix * FSOUND_OOMixRate * 1000
					long tmp;
					tmp = (long)samplestomix * 20;
					_mod.time_ms += (uint)(tmp / (FSOUND_MixRate / 50)); ;
				}

				samplesleft -= samplestomix;

			} while (blocksizetomix != 0);

			{
				uF_STATS stats;

				module.mixer_samplesleft = samplesleft;

				stats = _mod.tInfo[_mod.uFMOD_FillBlk];
				stats.s_row = (ushort)module.row;
				stats.s_order = (ushort)module.order;
			}

		do_swfill:
			{
				uint[] mixBuf = _mod.MixBuf;
				int mixBufi = 0;

				// ebx: L channel RMS volume
				// ebp: R channel RMS volume
				uint Lvol = 0;
				uint samplehi = 0;
				uint blk = _mod.uFMOD_FillBlk;
				blk <<= FSOUND_Block + 2;
				//ushort* databuff = (short*)((char*)_mod.databuf + blk);
				ushort[] databuff = _mod.databuf;
				int databuffi = (int)(blk>>1);
				//Array.Clear(databuff, databuffi, FSOUND_BlockSize*2);
				uint ecx = FSOUND_BlockSize * 2;
				int edi;
				int esi;
				uint Rvol = 0;
				long tmp;
				uint samplelo;

				//fill_loop_2:
				do
				{
					samplelo = mixBuf[mixBufi++];
					samplehi = cdq(samplelo);
					edi = (int)samplelo;
					samplelo ^= samplehi;
					esi = (255 * volumerampsteps) / 2;
					samplelo -= samplehi;
					tmp = samplelo;
					samplelo = (uint)(tmp / esi);
					samplehi = (uint)(tmp % esi);
					int C = (samplehi < (255 * volumerampsteps / 4))?1:0;
					samplelo -= (uint)(-1 + C);
					C = (samplelo < 0x8000)?1:0;
					samplehi -= (uint)(samplehi + C);
					samplehi = ~samplehi;
					samplelo |= samplehi;
					edi >>= 31;
					samplelo &= 0x7fff;
					tmp = (long)samplelo * _mod.ufmod_vol;
					samplelo = (uint)tmp;
					samplehi = (uint)(tmp >> 32);
					samplelo >>= 15;

					// sum.the L and R channel RMS volume
					//ecx = rotate_right32(ecx, 1);
					samplehi -= samplehi + (ecx & 1);
					samplehi &= samplelo;
					Rvol += samplehi; // += |vol|
									  //ecx = rotate_left32(ecx, 1);
					samplehi -= samplehi + (ecx & 1);
					samplehi = ~samplehi;
					samplehi &= samplelo;
					Lvol += samplehi; // += |vol|
					samplelo ^= (uint)edi;
					samplelo -= (uint)edi;
					edi = esi;

					databuff[databuffi++] = (ushort)samplelo;

					ecx--;
				} while (ecx != 0);

				//SW_Fill_Ret:

				blk = _mod.uFMOD_FillBlk;
				blk++;
				if (blk >= totalblocks)
					blk = 0;

				//SW_Fill_R:
				_mod.uFMOD_FillBlk = blk;
				_mod.tInfo[blk].L_vol = (ushort)(Lvol >> FSOUND_Block);
				_mod.tInfo[blk].R_vol = (ushort)(Rvol >> FSOUND_Block);
			}
#if USE_DUMP
			Dump.dumpstate($"{frameCount++,5}", _mod);
#endif
		}

		private static readonly byte[] sin127 =
		{
			0x00, 0x0C, 0x19, 0x25, 0x31, 0x3C, 0x47, 0x51, 0x5A, 0x62, 0x6A, 0x70, 0x75, 0x7A, 0x7D, 0x7E,
			0x7F, 0x7E, 0x7D, 0x7A, 0x75, 0x70, 0x6A, 0x62, 0x5A, 0x51, 0x47, 0x3C, 0x31, 0x25, 0x19, 0x0C
		};

		private static readonly byte[] sin64 =
		{
			0x00, 0x02, 0x03, 0x05, 0x06, 0x08, 0x09, 0x0B, 0x0C, 0x0E, 0x10, 0x11, 0x13, 0x14, 0x16, 0x17,
			0x18, 0x1A, 0x1B, 0x1D, 0x1E, 0x20, 0x21, 0x22, 0x24, 0x25, 0x26, 0x27, 0x29, 0x2A, 0x2B, 0x2C,
			0x2D, 0x2E, 0x2F, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x38, 0x39, 0x3A, 0x3B,
			0x3B, 0x3C, 0x3C, 0x3D, 0x3D, 0x3E, 0x3E, 0x3E, 0x3F, 0x3F, 0x3F, 0x40, 0x40, 0x40, 0x40, 0x40,
			0xAB /* sometimes this is read off the end of the array in asm version */
		};

		private const float f0_0833 = 8.3333336e-2f;
		private const float f13_375 = 1.3375e1f;
		private const float f0_0013 = 1.302083375e-3f;
		private const float f8363_0 = 8.3630004275e3f;

		private void Ramp(uF_MOD module, int lengthX, uint[] mixbuff, int mixbufferX, ref int mixbuffend)
		{
			FSOUND_CHANNEL channel;
			uF_SAMP sample;

			for (int i = 0; i < _mod.module.Channels.Length; i++)
			{
				channel = _mod.module.Channels[i];

				//loop_ch:
				sample = channel.fsptr;
				if (sample == null)
					goto MixExit;

				//CalculateLoopCount:
				int mixbuffer = mixbufferX;
				uint length = (uint)lengthX;
				do
				{
					for (;;)
					{
						int c = Ramp_calculate_loop_count(sample, channel, ref length);

						if (c == -1)
							goto MixExit;
						if (c == 1)
							break; //jmp DoOutputbuffEnd

						if (Ramp_mix(sample, channel, ref length, mixbuff, ref mixbuffer, mixbuffend) != 0)
							break; //jmp DoOutputbuffEnd
					}

					//DoOutputbuffEnd:
					if (_mod.mix_endflag[0] != 0)
						goto MixExit;

					if (Ramp_update_loops(sample, channel)!=0)
						break; //jmp MixExit

				} while (length != 0);

			MixExit: ;
			//channel++;

			} 
		}

		private int Ramp_calculate_loop_count(uF_SAMP sample, FSOUND_CHANNEL channel, ref uint length)
		{
			uint mixposlo;
			uint speedlo;
			int mixposhi;
			int speedhi;
			int edx;
			long tmp;

			// Set up a mix counter.See what will happen first, will the output buffer
			// end be reached first ? or will the end of the sample be reached first ? whatever
			// is smallest will be the mixcount.

			mixposhi = (int)channel.mixpos;
			edx = (int)sample.loopstart;
			mixposlo = channel.mixposlo;
			if (channel.speeddir == 0)
			{
				// work out how many samples left from mixpos to loop end
				edx += (int)sample.looplen;
				edx -= mixposhi;
				if (edx <= 0)
				{
					edx = (int)sample._length;
					edx -= mixposhi;
				}
				//submixpos:
				// edx: samples left(loopstart + looplen - mixpos)
				edx = -edx;
				mixposlo = (~mixposlo) + 1;
				edx += mixposhi + ((mixposlo != 0)?1:0);
			}

			//samplesleftbackwards:
			// work out how many samples left from mixpos to loop start

			edx = -edx;
			edx += mixposhi;
			if (edx < 0)
				return -1;//js MixExit

			// edx:eax now contains number of samples left to mix
			speedlo = channel.speedlo;
			mixposlo = shrd(mixposlo, (uint)edx, 5);
			speedhi = channel.speedhi;
			edx >>= 5;
			speedlo = shrd(speedlo, (uint)speedhi, 5);
			if (speedlo == 0)
			{
				channel.speedlo = FREQ_40HZ_f;
				speedlo = FREQ_40HZ_p;
			}
			//speedok:
			tmp = ((long)edx << 32) + mixposlo;
			mixposlo = (uint)(tmp / speedlo);
			edx = (int)(tmp % speedlo);
			speedlo = 0;
			mixposlo += (uint)(speedlo + ((edx != 0)?1:0));
			if (mixposlo == 0)
				return 1; //jz DoOutputbuffEnd

			// set a flag to say mix will end when end of output buffer is reached
			_mod.mix_endflag[0] = (byte)((mixposlo > length)?1:0);

			if (mixposlo < length)
				length = mixposlo;

			//staywithoutputbuffend:
			{
				uint rampcount;
				//uint mixposhi;
				int leftvol;
				int rightvol;

				rampcount = channel.ramp_count;
				// VOLUME RAMP SETUP
				// Reasons to ramp
				// 1 volume change
				// 2 sample starts(just treat as volume change - 0 to volume)
				// 3 sample ends(ramp last n number of samples from volume to 0)
				// now if the volume has changed, make end condition equal a volume ramp
				leftvol = (int)channel.leftvolume;
				rightvol = (int)channel.rightvolume;
				rightvol <<= 16;
				rightvol |= leftvol;
				_mod.mmf_mixcount = length; //remember mix count before modifying it //todo check

				if (rampcount == 0)
					goto volumerampstart;

				if (rightvol == channel.ramp_LR_target)
				{
					//goto volumerampclamp;
					if (length <= rampcount)
						goto volumerampend;

					length = rampcount;
					goto volumerampend;
				}

				volumerampstart:
				// SETUP NEW RAMP
				channel.ramp_LR_target = (uint)rightvol;

				leftvol <<= volumeramps_pow;
				leftvol -= (int)channel.ramp_leftvolume;
				leftvol >>= volumeramps_pow;
				channel.ramp_leftspeed = leftvol;
				if (leftvol != 0)
					rampcount = volumerampsteps;

				//novolumerampL:
				rightvol = (int)(rightvol & 0xffff0000);
				rightvol >>= (16 - volumeramps_pow);
				rightvol -= (int)channel.ramp_rightvolume;
				rightvol >>= volumeramps_pow;
				channel.ramp_rightspeed = rightvol;
				if (rightvol != 0)
					rampcount = volumerampsteps;

				//novolumerampR:
				channel.ramp_count = (ushort)rampcount;
				if (rampcount != 0)
				{
				volumerampclamp:
					if (length <= rampcount)
						goto volumerampend;

					length = rampcount;
				}
			volumerampend:

				_mod.ramp_leftspeed = channel.ramp_leftspeed;
				_mod.ramp_rightspeed = channel.ramp_rightspeed;
				_mod.mmf_length = length;//todo
			}
			return 0;
		}

		private int Ramp_mix(uF_SAMP sample, FSOUND_CHANNEL channel, ref uint length, uint[] mixbuffer, ref int mixbufferi, int endofmixbuffer)
		{
			// SET UP ALL OF THE REGISTERS HERE FOR THE INNER LOOP
			// edx: speed
			// ebx: mixpos
			// ebp: speed low
			// esi: destination pointer
			// edi: counter

			//int mbpi = 0;
			//short* mixpos;
			//mixpos = (short*)((char*)sample + channel.mixpos * 2 + offsetof(uF_SAMP, buff));
			ushort[] mixpos = sample.buff;
			int mpi = (int)channel.mixpos;

			//todo, refactor into 64bit math
			int speedhi = channel.speedhi;
			uint speedlo = channel.speedlo;

			if (channel.speeddir == 0)
				goto MixLoop16;

			speedlo = (~speedlo) + 1;
			speedhi = -speedhi - ((speedlo != 0)?1:0);

		MixLoop16:
			while (length-- != 0)
			{
				int s0, s1;
				uint mixposlo;
				long tmp;
				int mix;
				uint s1lo;

				s0 = (short)mixpos[mpi];
				s1 = (short)mixpos[mpi+1];

				// interpolate between s0 and s1 by mixposlo
				mixposlo = channel.mixposlo;
				s1 -= s0;
				mixposlo >>= 1;//31 bit unsigned

				tmp = s1 * (long)mixposlo;//edx:eax = 31+16 bits signed
				s1 = (int)((tmp << volumeramps_pow) >> 32);//31+16+7-32 = 16+6 bits signed
				s0 <<= volumeramps_pow - 1;//16+6 bits signed 
				mix = s0 + s1;

				tmp = mix * (long)channel.ramp_rightvolume;//edx:sx = 31+16 bits signed
				s1 = (int)(tmp >> (volumeramps_pow - 1));//31+16-6-32 = 31+10-32 = 9 bits signed
				s1lo = rotate_left32((uint)(tmp >> 32), 1);
				s1lo &= 1;//sign bit of s1
				s1 += (int)s1lo;
				s1 >>= 1;
				mixbuffer[mixbufferi++] += (uint)s1;

				tmp = mix * (long)channel.ramp_leftvolume;
				s1 = (int)(tmp >> (volumeramps_pow - 1));
				s1lo = rotate_left32((uint)(tmp >> 32), 1);
				s1lo &= 1;
				s1 += (int)s1lo;
				s1 >>= 1;
				mixbuffer[mixbufferi++] += (uint)s1;

				channel.ramp_rightvolume += (uint)_mod.ramp_rightspeed;
				channel.ramp_leftvolume += (uint)_mod.ramp_leftspeed;

				{
					ulong tmp2 = (ulong)channel.mixposlo + speedlo;
					channel.mixposlo = (uint)tmp2;
					//mixpos += speedhi + (int)(tmp2 >> 32);
					mpi += speedhi + (int)(tmp2 >> 32);
				}
			}

			// find out how many OUTPUT samples left to mix
			//length = (uint)(ptrdiff_t)((char*)endofmixbuffer - (char*)mixbuffer) / 8;
			length = (uint)((endofmixbuffer - mixbufferi) / 2);

			Debug.Assert((int)length >= 0);

			//turn mixpos pointer back into a sample number (when it's signed, top bit must be cleared by shift)
			//channel.mixpos = (uint)((char*)mixpos - (char*)&sample.buff) >> 1;
			channel.mixpos = (uint)mpi;

			//*mixbufferptr = mixbuffer;

			{
				// DID A VOLUME RAMP JUST HAPPEN ?
				int ramp;
				int rampcount;

				rampcount = channel.ramp_count;
				if (rampcount == 0)
					return 1;

				ramp = (int)_mod.mmf_length;// length
				channel.ramp_count -= (ushort)ramp;
				if (channel.ramp_count != 0)
					return 1;

				ramp -= (int)_mod.mmf_mixcount;// mixcount
				channel.ramp_leftspeed = 0;
				channel.ramp_rightspeed = 0;
				rampcount -= rampcount + ((ramp != 0)?1:0);
				if ((rampcount & length) == 0)
					return 1;
			}

			return 0;
		}

		private int Ramp_update_loops(uF_SAMP sample, FSOUND_CHANNEL channel)
		{
			int loopmode;
			uint speedlo;
			uint mixpos;
			//DoOutputbuffEnd:

			mixpos = channel.mixpos;
			speedlo = channel.speedlo;

			//00 - no loop
			//01 - loop forward
			//10 - ping-pong (bi-directional)
			//11 - reserved

			loopmode = sample.loopmode;

			// SWITCH ON LOOP MODE TYPE
			loopmode--;
			if (loopmode == 0)// check for normal loop(FSOUND_LOOP_NORMAL = 1)
			{
				int rampcount = 0;
				uint loopstart = sample.loopstart;
				uint looplen = sample.looplen;
				loopstart += looplen;
				if (mixpos > loopstart)
				{
					uint tmp_3;
					mixpos -= loopstart;
					tmp_3 = loopstart;
					loopstart = mixpos;
					mixpos = tmp_3;
					rampcount = (int)(loopstart % looplen);
				}
				//rewind_ok:
				looplen -= (uint)rampcount;
				mixpos -= looplen;
				if (mixpos <= 0)
					mixpos = 0;
				goto ChkLoop_OK;
			}

			//CheckBidiLoop:
			loopmode--; // FSOUND_LOOP_BIDI = 2

			if (loopmode != 0)
			{
				channel.mixposlo = 0;
				channel.mixpos = 0;
				channel.fsptr = null;
				return 1;//jz MixExit
			}

			if (channel.speeddir == loopmode) // FSOUND_MIXDIR_FORWARDS
				goto BidiForward;

			//BidiBackwards:
			speedlo = (~speedlo) + 1;
			
			bidiloop:
			
				uint loopstart2;
				uint loopend_m1;
				ulong tmp;
				int overflow;

				loopstart2 = sample.loopstart;
				speedlo = (~speedlo) + 1;
				loopstart2--;
				speedlo--;
				channel.speeddir--; // set FSOUND_MIXDIR_FORWARDS
				loopstart2 -= (uint)(mixpos + ((speedlo == 0xffffffff)?1:0));
				mixpos = sample.loopstart;
				mixpos += loopstart2;
				if ((int)loopstart2 < (int)sample.looplen)
					goto BidiFinish;

				BidiForward:
				tmp = (ulong)sample.loopstart;
				tmp += sample.looplen;
				loopstart2 = (uint)tmp;
				overflow = (int)(tmp >> 32);
				loopend_m1 = loopstart2 - 1;
				loopstart2 -= (uint)(mixpos + overflow);
				speedlo = (~speedlo) + 1;
				mixpos = loopstart2;
				speedlo--;
				overflow = (speedlo == 0xffffffff)?1:0;
				mixpos += (uint)(loopend_m1 + overflow);
				channel.speeddir++;// go backwards
				if (mixpos >= sample.loopstart)
					goto BidiFinish;
			goto bidiloop;
			BidiFinish:

			channel.mixposlo = speedlo;

		ChkLoop_OK:
			channel.mixpos = mixpos;

			return 0;
		}

		//work out difference between two pitches, and lerp with the finetune value
		private static int AmigaPeriod(uF_SAMP sample, uint note)
		{
			int eax;
			int edx, ecx;
			uint edi, esi;
			int tmp;

			esi = 1;
			edx = (int)(132 - note);
			tmp = edx;

			eax = sample.finetune;
			edi = (uint)edx;

			if (note != 0)
				edx = (int)(cdq((uint)eax) << 1);

			//_do_inc:
			edx++;

			//exp2:
			do
			{

				/*
				__asm ("fild dword [tmp]");
				__asm ("fmul dword [f0_0833]");
				__asm ("fld st(0)");
				__asm ("frndint");
				__asm ("fsub st(1), st(0)");
				__asm ("fxch st(1)");
				__asm ("f2xm1");
				__asm ("fld1");
				__asm ("faddp st(1)");
				__asm ("fscale");
				__asm ("fstp st(1)");
				__asm ("fmul dword [f13_375]");
				__asm ("fistp dword [tmp]");
				*/

				/*
				multiply by 0.0833
				separate fraction and int parts of number
				add the powers of 2 of each part
				multiply by 13.375

				return (int)(f13_375 * powf(2.0f, (float)tmp * f0_0833));
				*/

				//tmp = (int)Math.Round(f13_375 * Math.Pow(2.0, tmp * f0_0833));
				tmp = FreqTab.pow2_table_AmigaPeriod(tmp);

				ecx = tmp;

				if (esi == 0)
					break;

				esi = 0;
				edi -= (uint)edx;

				tmp = (int)edi;

				edi = (uint)ecx;
			} while (true);

			//exp2_end:
			{
				int offset;
				offset = (int)(ecx - edi);
				if (edx < 0)
					offset = -offset;

				//_do_imul:
				long tmp2 = (long)eax * offset;
				eax = (int)tmp2;
				edx = (int)((tmp2 >> 32) & 0x7f);
				eax += edx;
				eax >>= 7;
				eax += (int)edi;
			}
			return eax;
		}

		private static int VibratoOrTremolo(/*uF_CHAN ufchan*/IVibratoOrTremolo ufchan, uint C2)
		{
			//uint C2;
			uint vibpos;
			int newvibpos;

			vibpos = ufchan.vibpos;
			//C2 = ufchan.wavecontrol;
			newvibpos = (int)vibpos;
			newvibpos += ufchan.vibspeed;
			newvibpos &= 0x3f;

			C2 &= 3;
			ufchan.vibpos = (byte)newvibpos;
			if (C2 != 0)
			{
				//C2: square
				vibpos = rotate_left32(vibpos, 0x1b);
				newvibpos -= newvibpos;
				newvibpos ^= 0x7f;
				newvibpos |= 1;
				C2--;
				if (C2 != 0)
					goto vibrato_default;

				//C1 : triangle
				newvibpos >>= 0x18;
				vibpos = rotate_right32(vibpos, 0x17);
				newvibpos += (int)vibpos;
				Debug.Assert(false);
				newvibpos = ~newvibpos;
			}
			else
			{
				//C0: sine wave
				newvibpos = sin127[vibpos & 0x1f];
				if ((vibpos & 0x20)!=0)
					newvibpos = -newvibpos;
			}

		vibrato_default:
			return (newvibpos * ufchan.vibdepth) >> 5;
		}

		private static void Vibrato(uF_CHAN ufchan)
		{
			int eax = VibratoOrTremolo(ufchan.Vibrato, ufchan.wavecontrol);
			eax >>= 1;
			ufchan.freqdelta = eax;
			ufchan.notectrl |= FMUSIC_FREQ;
		}

		private static void Tremolo(uF_CHAN ufchan)
		{
			
			//There's a bug here in the assembler version.
			//A cheat is made there to re-use VibratoOrTremolo for Tremolo, which offsets the 'ufchan' channel pointer
			//by enough (3 bytes) to make that function read the Tremolo values but using the Vibrato offsets.

			//hacky!
			//uF_CHAN hackyufchan = (uF_CHAN*)((char*)ufchan + offsetof(uF_CHAN, tremolopos) - offsetof(uF_CHAN, vibpos));

			//Unfortunately VibratoOrTremolo ALSO accesses 'wavecontrol' using that offset pointer, which means
			//it picks up a value 3 bytes further on.  This is actually the 'fineportadown' value.
			//For compatibility, we pass fineportadown.

			//correct value
			//int eax = VibratoOrTremolo(ufchan.Tremolo, (uint)(ufchan.wavecontrol>>4));
			
			//bug compatibility with ufmod.asm 1.25.2a
			int eax = VibratoOrTremolo(ufchan.Tremolo, ufchan.fineportadown);
			
			ufchan.voldelta = eax;
			ufchan.notectrl |= FMUSIC_VOLUME;
		}

		private static void Portamento(uF_CHAN ufchan)
		{
			int freq, portatarget, portaspeed;

			ufchan.notectrl |= FMUSIC_FREQ;

			freq = (int)ufchan.freq;
			portatarget = (int)ufchan.portatarget;
			portaspeed = ufchan.portaspeed;
			portaspeed <<= 2;

			freq -= portatarget;

			if (freq > 0)
				goto porta_do_sub;
			freq += portaspeed;

			if (freq > 0)
				goto _do_trim;
			goto _no_trim;

		porta_do_sub:
			freq -= portaspeed;
			if (freq < 0)
				goto _do_trim;

			_no_trim:
			portatarget += freq;

		_do_trim:
			ufchan.freq = (uint)portatarget;
		}

		private static void Envelope(uF_CHAN ufchan, uF_INST instrument, uint eax)
		{
			uint ebx, panvol_numpoints, panvol_type;
			uint tmp_0, edi, ecx;
			uint env_pos;
			int env_next;

			sbyte sustain_l1;
			sbyte sustain_l2;
			sbyte sustain_loop;

			uF_VOLPAN vp;
			uF_VOLPANFRAC vpf;
			byte vps;
			ushort[] pts;

			ebx = 0;

			ufchan.notectrl |= (byte)eax;

			sustain_loop = (sbyte)instrument.PANsustain;
			sustain_l1 = (sbyte)instrument.PANLoopStart;
			sustain_l2 = (sbyte)instrument.PANLoopEnd;
			panvol_type = instrument.PANtype;
			panvol_numpoints = instrument.PANnumpoints;

			vp = ufchan.envpan;
			vpf = ufchan.envpanfrac;
			vps = ufchan.envpanstopped;
			pts = instrument.PANPoints;

			bool is_pan = true;

			if (eax == FMUSIC_VOLUME)
			{
				sustain_loop = (sbyte)instrument.VOLsustain;
				sustain_l1 = (sbyte)instrument.VOLLoopStart;
				sustain_l2 = (sbyte)instrument.VOLLoopEnd;
				panvol_type = instrument.VOLtype;
				panvol_numpoints = instrument.VOLnumpoints;

				vp = ufchan.envvol;
				vpf = ufchan.envvolfrac;
				vps = ufchan.envvolstopped;
				pts = instrument.VOLPoints;

				is_pan = false;
			}

			//pan_or_vol_ok:
			if (vps == 0)
			{
				// if (*pos >= numpoints) envelop out of bound
				uint pos = vp.pos;
				ushort tick;
				if (pos >= panvol_numpoints)
					goto envelope_done;

				//if (*tick == points[(*pos) << 1]) we are at the correct tick for the position
				tick = pts[pos * 2];
				if (vp.tick != tick)
					goto add_envdelta;

				if ((panvol_type & FMUSIC_ENVELOPE_LOOP) != 0)
				{
					// if((type&FMUSIC_ENVELOPE_LOOP) && *pos == loopend) handle loop
					if (vp.pos != sustain_l2)
						goto loop_ok;

					vp.pos = (uint)sustain_l1;
					vp.tick = pts[vp.pos * 2];
				}
			loop_ok:
				{
					uint pos2 = vp.pos;
					env_pos = pos2;
					//ushort* points = &pts[pos2 * 2];
					ushort[] points = pts;
					int pointsi = (int)(pos2 * 2);

					uint ebx2;

					panvol_numpoints--;//edx
					//ebx2 = *points;// get tick at this point
					ebx2 = points[pointsi];

					//edi = *(uint*)(points + 2);
					edi = (uint)((points[pointsi+2])+(points[pointsi + 2 + 1]<<16));
					//eax = *(points + 1);
					eax = points[pointsi + 1];

					env_next = (int)edi;// get tick at next point

					vpf.val = (int)eax;

					if (vp.pos != panvol_numpoints)
						goto env_continue;

					//(*vps)++;
					if (is_pan) ufchan.envpanstopped++;
					else ufchan.envvolstopped++;
				}
			}
			//goto_envelope_ret:
			goto Envelope_Ret;

		env_continue:
			eax <<= 16;
			edi -= eax;
			tmp_0 = eax;
			eax = edi;
			edi = tmp_0;
			eax &= 0xffff0000;
			if ((panvol_type & FMUSIC_ENVELOPE_SUSTAIN) != 0)
			{
				panvol_numpoints = (uint)sustain_loop;
				if (env_pos != panvol_numpoints)
					goto not_sustain;

				if (ufchan.keyoff == 0)
					goto Envelope_Ret;
			}
		not_sustain:
			// interpolate 2 points to find delta step

			vp.pos++;
			vpf.frac = (int)edi;
			ecx = (uint)(env_next & 0x0000ffff);
			vp.delta = 0;
			ecx -= vp.tick;
			if (ecx != 0)
			{
				vp.delta = (uint)((int)eax / (int)ecx);
				goto envelope_done;
			}

		add_envdelta:

			// interpolate
			vpf.frac += (int)vp.delta;

		envelope_done:

			vp.tick++;
			vpf.val = vpf.frac >> 16;// *value = *valfrac >> 16
		Envelope_Ret:;
		}

		private static void VolByte(uF_CHAN ufchan, int volparam)
		{
			uint tmp_0;

			uint volcommand;

			volparam -= 0x10;
			if (volparam >= 0 && volparam <= 0x40)
				ufchan.volume = volparam;

			int parm = volparam;
			//todo refactor into switch
			//switch_volume:
			volcommand = (uint)volparam;
			volparam &= 0xf;
			volcommand >>= 4;
			volcommand -= 5;
			if (volcommand != 0)
			{
				volcommand--;
				if (volcommand == 0)
				{
					goto case_7;
				}
				volcommand--;
				if (volcommand == 0)
				{
					goto case_6;
				}
				volcommand--;
				if (volcommand == 0)
				{
					goto case_7;
				}
				volcommand -= 2;
				if (volcommand == 0 || volcommand == 0xffffffff)
				{
					goto case_AB;
				}
				volcommand--;
				if (volcommand == 0)
				{
					goto case_C;
				}
				volcommand--;
				if (volcommand == 0)
				{
					goto case_D;
				}
				volcommand--;
				if (volcommand == 0)
				{
					goto case_E;
				}
				volcommand--;
				if (volcommand == 0)
				{
					volparam <<= 4;
					if (volparam != 0)
					{
						ufchan.portaspeed = (byte)volparam;
					}
					//vol_z:
					ufchan.notectrl &= NOT_FMUSIC_TRIGGER;
					volcommand = ufchan.period;
					ufchan.portatarget = volcommand;
				}
				//vol_default:
				return;
			}
		case_6:
			volparam = -volparam;
		case_7:
			ufchan.volume += volparam;
			return;

		case_AB:
			//command is 0 or -1
			if (volcommand == 0xffffffff)
				ufchan.vibpos = (byte)volparam;
			else
				ufchan.vibspeed = (byte)volparam;
			return;

		case_C:
			volparam <<= 4;
			ufchan.pan = (uint)volparam;
			tmp_0 = volcommand;
			volcommand = (uint)volparam;
			volparam = (int)tmp_0;
		case_D:
			volparam = -volparam;
		case_E:
			ufchan.pan += (uint)volparam;
			ufchan.notectrl |= FMUSIC_PAN;
		}

		private static void Tremor(uF_CHAN ufchan)
		{
			//int ecx;
			byte dl, dh;
			ufchan.notectrl |= FMUSIC_VOLUME;
			dl = ufchan.tremorpos;
			dh = ufchan.tremoron;
			if (dl <= dh)
				goto inc_pos;

			ufchan.voldelta = -ufchan.volume;

		inc_pos:
			dh += ufchan.tremoroff;
			if (dl <= dh)
				goto Tremor_Ret;
			dl = 0xff;
		Tremor_Ret:
			dl++;
			ufchan.tremorpos = dl;
		}

		private static void SetBPM(int bpm)
		{
			uint eax = (FSOUND_MixRate * 5) / 2;
			if (bpm != 0)
				eax /= (uint)bpm;
			_mod.module.mixer_samplespertick = eax;
		}

		private void uFMOD_readb(out byte b)
		{
			var bb = new byte[1];
			_mod.uFMOD_fread(bb, 1);
			b = bb[0];
		}
		private void uFMOD_readb(out sbyte b)
		{
			var bb = new byte[1];
			_mod.uFMOD_fread(bb, 1);
			b = (sbyte)bb[0];
		}
		private void uFMOD_readw(out ushort w)
		{
			var bb = new byte[2];
			_mod.uFMOD_fread(bb, 2);
			w = (ushort)(bb[0]+(bb[1]<<8));
		}
		private void uFMOD_readw(out short w)
		{
			var bb = new byte[2];
			_mod.uFMOD_fread(bb, 2);
			w = (short)(bb[0]+(bb[1]<<8));
		}
		private void uFMOD_readl(out uint i)
		{
			var bb = new byte[4];
			_mod.uFMOD_fread(bb, 4);
			i = (uint)(bb[0]+(bb[1]<<8)+(bb[2]<<16)+(bb[3]<<24));
		}
		private void uFMOD_readl(out int i)
		{
			var bb = new byte[4];
			_mod.uFMOD_fread(bb, 4);
			i = bb[0]+(bb[1]<<8)+(bb[2]<<16)+(bb[3]<<24);
		}
		private void uFMOD_reada(byte[] b, int len)
		{
			_mod.uFMOD_fread(b, (uint)len);
		}
		private void uFMOD_reada(ushort[] b, int len)
		{
			var bb = new byte[len * 2];
			_mod.uFMOD_fread(bb, (uint)(len*2));
			for (int i = 0; i < len; i++)
				b[i] = (ushort)(bb[i * 2] + (bb[i * 2 + 1] << 8));
		}
		private void uFMOD_reads(out string s, int len)
		{
			var bb = new byte[len];
			_mod.uFMOD_fread(bb, (uint)bb.Length);
			var sb = new StringBuilder();
			for (int i = 0; i < bb.Length; i++)
			{
				if (bb[i] > 32)
					sb.Append((char)bb[i]);
				else
					break;
			}
			s = sb.ToString();
		}


		// definitive XM format, thanks to the guys at MilkyTracker!
		// https://github.com/milkytracker/MilkyTracker/blob/master/resources/reference/xm-form.txt
		// https://www.fileformat.info/format/xm/corion.htm
		private int LoadXM(byte[] lpXM)
		{
			int channelcount;
			int loadxm_fnumpat;
			uint loadxm_numpat;

			uF_MOD module;
			uF_XM xm = new uF_XM();

			uint numpat = 0;

			_mod.uFMOD_fopen(lpXM);
			if (_mod.SW_Exit == null)
				return 1;

			module = _mod.module = new uF_MOD();

			//read the XM and module headers
			//_mod.uFMOD_fread(&xm, uF_XM_size);
			//swabufxm(&xm);

			uFMOD_reads(out xm.idtext, 17);
			uFMOD_reads(out xm.modulename, 20);
			uFMOD_readb(out xm.oneA);
			uFMOD_reads(out xm.trackername, 20);
			uFMOD_readw(out xm.version);

			//_mod.uFMOD_fread(&module.header_size, uF_MOD_size - offsetof(uF_MOD, header_size));
			//swabufmod(&module);

			uFMOD_readl(out module.header_size);

			uFMOD_readw(out module.numorders);
			uFMOD_readw(out module.restart);
			uFMOD_readb(out module.numchannels_xm);
			uFMOD_readb(out module.globalvsl);
			uFMOD_readw(out module.numpatternsmem);
			uFMOD_readw(out module.numinsts);
			uFMOD_readw(out module.flags);
			uFMOD_readw(out module.defaultspeed);
			uFMOD_readw(out module.defaultbpm);
			uFMOD_reada(module.orderlist, 256);

			// GOTO PATTERN DATA
			uFMOD_lseek((int)(module.header_size + uF_XM_size), 0, UFMOD_LSEEK_SET);

			_mod.szTtl = xm.modulename;

			{
				// COUNT NUM.OF PATTERNS
				int numorders = module.numorders;
				uint fnumpat = module.numpatternsmem;

				if (fnumpat == 0)
					numorders = 0;

				numorders--;
				loadxm_fnumpat = (int)fnumpat;

				if (numorders < 0 || numorders > 255)
					return 1;

				numpat = module.orderlist.Take(numorders+1).Max();
				loadxm_numpat = numpat;
				numpat++;

				// ALLOCATE THE PATTERN ARRAY (whatever is bigger : fnumpat or numpat) & CHANNEL POOL
				if (fnumpat > numpat)
					numpat = fnumpat;
			}

			//uint patternsize;
			module.numpatternsmem = (ushort)numpat;
			//patternsize = numpat * uF_PAT_size;

			//loadxm_pats_ok2:
			{
				//uint instsize = module.numinsts * uF_INST_size;
				//uint buffsize = instsize + numpat * uF_PAT_size;
				//byte* buff;
				//uint channelsize;
				FSOUND_CHANNEL[] channel;
				uF_CHAN[] ufchan;

				uint numchannels = module.numchannels_xm;

				if (numchannels > 64)
					numchannels = 0;

				//loadxm_numchan_ok:
				module.numchannels = numchannels;
				if (numchannels == 0)
					return 1;

				//channelsize = numchannels * FSOUND_CHANNEL_size * 2;

				//buffsize += channelsize + numchannels * uF_CHAN_size;

				//buff = alloc(buffsize);

				//module.Channels = channel = (FSOUND_CHANNEL*)buff;
				//module.uFMOD_Ch = ufchan = (uF_CHAN*)(buff + channelsize);
				module.Channels = channel = new FSOUND_CHANNEL[numchannels * 2];
				module.uFMOD_Ch = ufchan = new uF_CHAN[numchannels];

				//loop_2:
				//channelcount = numchannels;
				//while (channelcount--)
				//{
				//	channel.speedhi = 1;
				//	ufchan.cptr = channel;

				//	channel += 2;
				//	ufchan++;
				//}
				for (int i = 0; i < numchannels; i++)
				{
					module.Channels[i*2] = new FSOUND_CHANNEL();
					module.Channels[i*2+1] = new FSOUND_CHANNEL();
					module.uFMOD_Ch[i] = new uF_CHAN();

					channel[i*2].speedhi = 1;
					ufchan[i].cptr = channel[i * 2];
				}

				//patterns follow channels
				//module.pattern = (uF_PAT*)ufchan;
				module.pattern = new uF_PAT[numpat];
				for (int i = 0; i < numpat; i++)
					module.pattern[i] = new uF_PAT();

				//instruments follow patterns
				//module.instrument = (uF_INST*)((char*)ufchan + patternsize);
				module.instrument = new uF_INST[module.numinsts];
				for (int i = 0; i < module.numinsts; i++)
					module.instrument[i] = new uF_INST();
			}

			SetBPM(module.defaultbpm);

			module.globalvolume = 64;
			module.speed = module.defaultspeed;

			loadxm_load_pats(module, loadxm_numpat, (uint)loadxm_fnumpat);
			loadxm_for_instrs(module);

#if USE_DUMP
			//Dump.dumpmod(_mod);
			//Dump.dumpmodule(_mod);
#endif

			return 0;
		}

		private void loadxm_load_pats(uF_MOD module, uint loadxm_numpat, uint loadxm_fnumpat)
		{
			uint loadxm_count1 = 0;
			uF_PAT[] pattern = module.pattern;
			int pi = 0;
			do
			{
				uF_NOTE []nptr;
				uF_PATTERN_HEADER pat_hdr = new uF_PATTERN_HEADER();

				//_mod.uFMOD_fread(&pat_hdr, uF_PATTERN_HEADER_size);
				//swabufpathdr(&pat_hdr);

				uFMOD_readl(out pat_hdr.headersize);
				uFMOD_readb(out pat_hdr.packingtype);
				uFMOD_readw(out pat_hdr.rowcount);
				uFMOD_readw(out pat_hdr.loadxm_pat_size);

				// ALLOCATE PATTERN BUFFER
				pattern[pi].patternsize = pat_hdr.loadxm_pat_size;
				pattern[pi].rows = (short)pat_hdr.rowcount;

				if (pat_hdr.loadxm_pat_size == 0) // skip an empty pattern
					goto loadxm_ldpats_continue;

				int loadxm_count2 = (int)(module.numchannels * pat_hdr.rowcount);

				//pattern[pi].data = alloc(loadxm_count2 * uF_NOTE_size);
				pattern[pi].data = new uF_NOTE[loadxm_count2];
				for (int i = 0; i < loadxm_count2; i++)
					pattern[pi].data[i] = new uF_NOTE();

				nptr = pattern[pi].data;
				int ni = 0;
				//loadxm_for_rowsxchan:
				do
				{
					byte note;
					uFMOD_readb(out note);
					if ((note & 0x80)!=0)
					{
						if ((note & 1)!=0)
							uFMOD_readb(out nptr[ni].unote);
						if ((note & 2)!=0)
							uFMOD_readb(out nptr[ni].number);
						if ((note & 4)!=0)
							uFMOD_readb(out nptr[ni].uvolume);
						if ((note & 8)!=0)
							uFMOD_readb(out nptr[ni].effect);
						if ((note & 16)!=0)
							uFMOD_readb(out nptr[ni].eparam);
					}
					else
					{
						if (note != 0)
							nptr[ni].unote = note;
						//_mod.uFMOD_fread(&nptr.number, 4);//read remaining unpacked note data
						uFMOD_readb(out nptr[ni].number);
						uFMOD_readb(out nptr[ni].uvolume);
						uFMOD_readb(out nptr[ni].effect);
						uFMOD_readb(out nptr[ni].eparam);
					}

					//loadxm_isnote97:
					if (nptr[ni].number > module.numinsts)
						nptr[ni].number = 0;
					//nptr++;
					ni++;
					loadxm_count2--;
				} while (loadxm_count2 != 0);

			loadxm_ldpats_continue:
				loadxm_count1++;
				//pattern++;
				pi++;
			} while (loadxm_count1 < loadxm_fnumpat);

			// allocate and clean out any extra patterns
			//if (loadxm_numpat >= loadxm_fnumpat)
			//{
				//uF_PAT extrapatterns;
				//uint emptypatternsize;

				//extrapatterns = module.pattern + loadxm_numpat;
				//emptypatternsize = module.numchannels * 64 * uF_NOTE_size;

				////loadxm_for_extrapats:
				//do
				//{
				//	extrapatterns.rows = 64;
				//	extrapatterns.data = alloc(emptypatternsize);
				//	extrapatterns--;
				//	loadxm_numpat--;
				//} while (loadxm_numpat >= loadxm_fnumpat);

				for (int i = (int)loadxm_fnumpat; i <= loadxm_numpat; i++)
				{
					module.pattern[i] = new uF_PAT {rows = 64, data = new uF_NOTE[module.numchannels * 64]};
					for (int j = 0; j < module.numchannels * 64; j++)
						module.pattern[i].data[j] = new uF_NOTE();
				}
				//}

			//loadxm_extrapats_ok:
			module.mixer_samplesleft = 0;
			module.tick = 0;
			module.patterndelay = 0;
			module.nextorder = 0;
			module.nextrow = 0;

			if (module.numinsts == 0)
				return;
		}

		private void loadxm_for_instrs(uF_MOD module)
		{
			int loadxm_count1;
			uF_INST instrument;

			for (loadxm_count1 = 0; loadxm_count1 < module.numinsts; loadxm_count1++)
			{
				instrument = module.instrument[loadxm_count1];
				uF_INSTRUMENT_HEADER inst_hdr=new uF_INSTRUMENT_HEADER();
				uint esi;
				uint dl;

				//_mod.uFMOD_fread(&inst_hdr, uF_INSTRUMENT_HEADER_size);
				//swabufinsthdr(&inst_hdr);
				uFMOD_readl(out inst_hdr.headersize);
				uFMOD_reads(out inst_hdr.instrumentname, 22);
				uFMOD_readb(out inst_hdr.instrumenttype);
				uFMOD_readw(out inst_hdr.samplecount);
				uFMOD_readl(out inst_hdr.sampleheadersize);

				esi = inst_hdr.headersize;
				dl = inst_hdr.samplecount;
				esi -= (uint)uF_INSTRUMENT_HEADER_size;

				if (dl != 0)
				{
					if (dl > 0x10)
						return;//todo

					if (inst_hdr.sampleheadersize > 40)
						return;//todo

					esi -= 208;
					//_mod.uFMOD_fread(&instrument.keymap, 208);//this is the remaining instrument header (241-33)
					//swabufinst(instrument);
					uFMOD_reada(instrument.keymap, 96);
					uFMOD_reada(instrument.VOLPoints, 24);
					uFMOD_reada(instrument.PANPoints, 24);
					uFMOD_readb(out instrument.VOLnumpoints);
					uFMOD_readb(out instrument.PANnumpoints);
					uFMOD_readb(out instrument.VOLsustain);
					uFMOD_readb(out instrument.VOLLoopStart);
					uFMOD_readb(out instrument.VOLLoopEnd);
					uFMOD_readb(out instrument.PANsustain);
					uFMOD_readb(out instrument.PANLoopStart);
					uFMOD_readb(out instrument.PANLoopEnd);
					uFMOD_readb(out instrument.VOLtype);
					uFMOD_readb(out instrument.PANtype);
					uFMOD_readb(out instrument.VIBtype);
					uFMOD_readb(out instrument.VIBsweep);
					uFMOD_readb(out instrument.iVIBdepth);
					uFMOD_readb(out instrument.VIBrate);
					uFMOD_readw(out instrument.VOLfade);
				}

				//loadxm_inst_ok:
				uFMOD_lseek((int)esi, 0, UFMOD_LSEEK_CUR);

				instrument.VOLfade = (ushort)(instrument.VOLfade << 1);

				if (instrument.VOLnumpoints < 2)
					instrument.VOLtype = 0;

				if (instrument.PANnumpoints < 2)
					instrument.PANtype = 0;

				//loadxm_PANtype_ok:
				if (inst_hdr.samplecount != 0)
				{
					loadxm_for_samp(instrument, inst_hdr.samplecount, (int)inst_hdr.sampleheadersize);
					loadxm_for_loadsamp(instrument, inst_hdr.samplecount);
				}
				//loadx_for_loadsamp_end:
			}
		}

		private void loadxm_for_samp(uF_INST instrument, int samplecount, int sampleheadersize)
		{
			int samplenumber;
			for (samplenumber = 0; samplenumber < samplecount; samplenumber++)
			{
				uF_SAMPLE_HEADER sample_hdr = new uF_SAMPLE_HEADER();
				byte is16;
				uF_SAMP sampledataptr = new uF_SAMP();

				//_mod.uFMOD_fread(&sample_hdr, sampleheadersize);//sizeof uF_SAMPLE_HEADER or less
				//swabufsamphdr(&sample_hdr);
				uFMOD_readl(out sample_hdr.loadxm_sample_2);
				uFMOD_readl(out sample_hdr.loadxm_s0loopstart);
				uFMOD_readl(out sample_hdr.loadxm_s0looplen);
				uFMOD_readb(out sample_hdr.volume);
				uFMOD_readb(out sample_hdr.finetune);
				uFMOD_reada(sample_hdr.loadxm_s0bytes, 4);
				uFMOD_reads(out sample_hdr.samplename, 22);

				sample_hdr.loadxm_s0loopmode = sample_hdr.loadxm_s0bytes[0];
				is16 = (byte)((sample_hdr.loadxm_s0loopmode >> 4) & 1);
				sample_hdr.loadxm_s0bytes[0] = is16;//todo: messy
				if (is16 != 0)
				{
					sample_hdr.loadxm_sample_2 >>= 1;
					sample_hdr.loadxm_s0loopstart >>= 1;
					sample_hdr.loadxm_s0looplen >>= 1;
				}

				//loadxm_s0bytes_ok:
				if (sample_hdr.loadxm_s0loopstart > sample_hdr.loadxm_sample_2)
					sample_hdr.loadxm_s0loopstart = sample_hdr.loadxm_sample_2;
				//loadxm_loopstart_ok:
				if (sample_hdr.loadxm_s0loopstart + sample_hdr.loadxm_s0looplen > sample_hdr.loadxm_sample_2)
					sample_hdr.loadxm_s0looplen = sample_hdr.loadxm_sample_2 - sample_hdr.loadxm_s0loopstart;

				//loadxm_looplen_ok:
				sample_hdr.loadxm_s0loopmode &= SAMPLE_LOOP_MASK;
				if (sample_hdr.loadxm_s0loopmode == SAMPLE_LOOP_TYPE_NONE || sample_hdr.loadxm_s0looplen == 0)
				{
					//there's a loop type, but the length of it is 0, or there's no loop
					sample_hdr.loadxm_s0loopstart = 0;
					sample_hdr.loadxm_s0looplen = sample_hdr.loadxm_sample_2;
					sample_hdr.loadxm_s0loopmode = 0;
				}

				//sampledataptr = alloc(sample_hdr.loadxm_sample_2 * 2 + uF_SAMP_size + 4);
				sampledataptr.buff = new ushort[sample_hdr.loadxm_sample_2];
				instrument.sample[samplenumber] = sampledataptr;

				//copy over the first 20 bytes of the header
				//ufmemcpy(eax, &sample_hdr, 20);
				sampledataptr._length = sample_hdr.loadxm_sample_2;//4
				sampledataptr.loopstart = sample_hdr.loadxm_s0loopstart;//8
				sampledataptr.looplen = sample_hdr.loadxm_s0looplen;//12
				sampledataptr.defvol = sample_hdr.volume;//13
				sampledataptr.finetune = (sbyte)sample_hdr.finetune;//14
				sampledataptr.bytes = sample_hdr.loadxm_s0bytes[0];//15  NB. this is modified from sample_hdr
				sampledataptr.defpan = sample_hdr.loadxm_s0bytes[1];//16
				sampledataptr.relative = (sbyte)sample_hdr.loadxm_s0bytes[2];//17
				sampledataptr.Resved = sample_hdr.loadxm_s0bytes[3];//18
				sampledataptr.loopmode = sample_hdr.loadxm_s0loopmode;//19
				sampledataptr._align = 0;//20
			}
		}

		private void loadxm_for_loadsamp(uF_INST instrument, int samplecount)
		{
			int samplenumber = 0;
			for (samplenumber = 0; samplenumber < samplecount; samplenumber++)
			{
				uF_SAMP sample;
				byte ch;
				byte cl;
				uint eax;
				int length;
				byte []sampledata;
				byte[] cmp=new byte[16];

				sample = instrument.sample[samplenumber];
				eax = 0;
				length = (int)sample._length;
				ch = sample.Resved;
				cl = sample.bytes;
				if ((length & 0xffc00000) != 0)//4,194,303 max sample size
					return;//todo

				//sampledata = (byte*)&sample.buff;

				if (ch != MODPLUG_4BIT_ADPCM) // ModPlug 4-bit ADPCM
					goto loadxm_regular_samp;

				// unpacked length
				length = ((length + 1) >> 1) * 3;
				//sampledata += length;

				// Read in the compression table
				_mod.uFMOD_fread(cmp, 16);

				// Read the sample data 
				sampledata = new byte[(sample._length + 1) >> 1];
				_mod.uFMOD_fread(sampledata, (sample._length + 1) >> 1);
				int si = 0;
				int di = 0;

				//loadxm_unpack_loop:
				{
					// Decompress sample data
					//uint* dst = (uint*)&sample.buff;
					int ecx = 0;

					for (;;)
					{
						byte al, ah;

						if (di >= sampledata.Length)
							break;

						al = sampledata[si++];
						ah = al;
						al &= 0xf;
						al = cmp[al];
						ah >>= 4;
						ch += al;//todo ch is part of ecx
						al = ah;
						al = cmp[al];
						al += ch;
						eax <<= 24;
						ecx |= (int)eax;
						sample.buff[di++] = (ushort)ecx;
						sample.buff[di++] = (ushort)(ecx>>16);
						ecx >>= 16;
					}
				}
				//loadxm_unpack_ok:

				goto loadxm_chk_loop_bidi;

			loadxm_regular_samp:

				if (sample.bytes != 0)
				{
					sample.buff = new ushort[sample._length+2];
					uFMOD_reada(sample.buff, (int)sample._length);

					//swabsample(sample.buff, sample._length);
					//goto loadxm_16bit_ok;
				}
				else
				// Promote to 16 bits
				//loadxm_to16bits:
				{
					//byte* src = sampledata + sample._length;
					//ushort* dst = (ushort*)(src + sample._length);

					//do
					//{
					//	*--dst = (ushort)*--src << 8;
					//} while ((byte*)dst > src);
					//sample.buff = sample.buff.SelectMany(x => new []{(ushort)(x<<8), (ushort)(x &0xff00)}).ToArray();
					var tmpSmp = new byte[sample._length+4]; 
					uFMOD_reada(tmpSmp, (int)sample._length);
					sample.buff = tmpSmp.Select(x => (ushort)(x << 8)).ToArray();
				}

			loadxm_16bit_ok:

				//loadxm_do_delta_conv:
				{
					ushort dx = 0;
					//ushort* src = (ushort*)sampledata;
					//int len = sample._length;

					// Do delta conversion
					//do
					//{
					//	dx += *src;
					//	*src++ = dx;
					//	len--;
					//} while (len > 0);
					for (int i = 0; i < sample._length; i++)
					{
						dx += sample.buff[i];
						sample.buff[i] = dx;
					}
				}
			//goto loadxm_loops_ok; // branch never taken in asm

			loadxm_chk_loop_bidi:
				{
					ushort cx;
					//ushort* src;

					//src = (ushort*)sampledata + sample.loopstart + sample.looplen;
					int src =  (int)(sample.loopstart + sample.looplen);

					if (sample.loopmode == 2) // LOOP_BIDI
					{
						//cx = src[-1]
						cx = sample.buff[src-1];
					}
					else
					{
						if (sample.loopmode != 1) // LOOP_NORMAL
						{
							goto loadxm_loops_ok;
						}
						//cx = *((ushort*)sampledata + sample.loopstart);
						cx = sample.buff[sample.loopstart];
					}
					//loadxm_fix_loop:
					sample.buff[src] = cx;
				}

			loadxm_loops_ok:;
			}
		}

		private uF_PAT DoNote(uF_MOD module, uF_PAT[] pattern)
		{
			uint numchannels;
			uF_NOTE[] noteptr;

			uF_INST donote_iptr;
			int donote_oldfreq;
			int donote_currtick;
			int donote_oldpan;
			uint donote_porta;
			int donote_jumpflag;
			uF_SAMP donote_sptr;

			uF_PAT patternptr;
			uF_CHAN ufchan;

			patternptr = pattern[module.orderlist[module.order]];

			numchannels = module.numchannels;
			donote_jumpflag = 0;

			if (patternptr.data == null)
				return patternptr;

			noteptr = patternptr.data;// + module.row * module.numchannels;
			int ni = (int)(module.row * module.numchannels);

			if (numchannels == 0)
				return patternptr;

			int ci = 0;

			//donote_for_channels:
			do
			{
				ufchan = module.uFMOD_Ch[ci];

				FMUSIC_XMCOMMANDS effect;
				int eparam, noteno, instno;

				eparam = noteptr[ni].eparam;
				effect = (FMUSIC_XMCOMMANDS)noteptr[ni].effect;
				eparam &= 0xf;
				instno = noteptr[ni].number;

				donote_porta = 0;
				if (effect == FMUSIC_XMCOMMANDS.FMUSIC_XM_PORTATO || effect == FMUSIC_XMCOMMANDS.FMUSIC_XM_PORTATOVOLSLIDE)
				{
					donote_porta = 1;
					goto donote_rem_note;
				}

				instno--;
				if (instno >= 0)
					ufchan.inst = (byte)instno;

				noteno = noteptr[ni].unote;
				noteno--;
				if (noteno >= 0 && noteno < 96)
					ufchan.note = (byte)noteno;

				donote_rem_note:;
				uint key;
				FMUSIC_XMCOMMANDS fx;

				donote_iptr = module.instrument[ufchan.inst];

				key = donote_iptr.keymap[ufchan.note];

				if (key < 16 && donote_iptr.sample[key] != null)
					donote_sptr = donote_iptr.sample[key];
				else
					donote_sptr = _mod.DummySamp;

				//donote_valid_sptr:

				donote_oldfreq = (int)ufchan.freq;
				donote_currtick = ufchan.volume;
				donote_oldpan = (int)ufchan.pan;

				// if there is no more tremolo, set volume to volume + last tremolo delta
				fx = (FMUSIC_XMCOMMANDS)noteptr[ni].effect;
				if (fx != FMUSIC_XMCOMMANDS.FMUSIC_XM_TREMOLO)
				{
					if (ufchan.recenteffect != (int)FMUSIC_XMCOMMANDS.FMUSIC_XM_TREMOLO)
					{
						goto donote_tremolo_vol;
					}
					ufchan.volume += ufchan.voldelta;
				}

			donote_tremolo_vol:
				ufchan.recenteffect = (byte)fx;
				ufchan.voldelta = 0;
				ufchan.freqdelta = 0;
				ufchan.notectrl = FMUSIC_VOLUME_OR_FREQ;
				// PROCESS NOTE
				
				{
					int note;
					note = noteptr[ni].unote;
					note--;
					if (note >= 0 && note < 96)
					{
						int edx, eax;
						note += donote_sptr.relative;
						ufchan.realnote = (byte)note;
						if ((_mod.module.flags & 1) != 0)
						{
							eax = donote_sptr.finetune;
							edx = (int)cdq((uint)eax);
							note <<= 6;
							eax -= edx;
							eax >>= 1;
							eax = note + eax - 0x1e00;
							eax = -eax;
						}
						else
						{
							eax = AmigaPeriod(donote_sptr, (uint)note);
						}

						ufchan.period = (uint)eax;

						if (donote_porta == 0)
							ufchan.freq = (uint)eax;

						ufchan.notectrl |= FMUSIC_TRIGGER;
					}
				}
				{
					// PROCESS INSTRUMENT NUMBER
					
					if (noteptr[ni].number != 0)
					{
						uF_SAMP sample;
						int wavecontrol;
						sample = donote_sptr;

						ufchan.volume = sample.defvol;
						ufchan.pan = sample.defpan;
						ufchan.envvolfrac.val = 64;
						ufchan.envpanfrac.val = 32;
						ufchan.fadeoutvol = 0x10000;

						//9 dwords from envvoltick
						//ufmemset(edi, eax, ecx);
						ufchan.envpan.tick = 0;
						ufchan.envpan.pos = 0;
						ufchan.envpan.delta = 0;
						ufchan.envvol.tick = 0;
						ufchan.envvol.pos = 0;
						ufchan.envvol.delta = 0;
						ufchan.ivibsweeppos = 0;
						ufchan.ivibpos = 0;
						ufchan.keyoff = 0;
						ufchan.envvolstopped = 0;
						ufchan.envpanstopped = 0;

						wavecontrol = ufchan.wavecontrol;

						if (wavecontrol < 0x4f)
							ufchan.tremolopos = 0;

						if ((wavecontrol & 0x0C) == 0)
							ufchan.vibpos = 0;

						ufchan.notectrl |= FMUSIC_VOLUME_OR_PAN;
					}
				}
				//donote_zcptr_ok:

				// PROCESS VOLUME BYTE
				
				VolByte(ufchan, noteptr[ni].uvolume);
				// PROCESS KEY OFF
				if (noteptr[ni].unote <= 96)
				{
					if (noteptr[ni].effect != (int)FMUSIC_XMCOMMANDS.FMUSIC_XM_KEYOFF)
						goto donote_keyoff_ok;
				}
				ufchan.keyoff++;

			donote_keyoff_ok:
				{
					// PROCESS ENVELOPES
					
					if ((donote_iptr.VOLtype & 1) != 0)
					{
						Envelope(ufchan, donote_iptr, FMUSIC_VOLUME);
					}
					else
					{
						if (ufchan.keyoff == 0)
							goto donote_volenv_ok;

						ufchan.envvolfrac.val = 0;
					}
				donote_volenv_ok:

					if ((donote_iptr.PANtype & 1) != 0)
					{
						Envelope(ufchan, donote_iptr, FMUSIC_PAN);
					}
				}

				//donote_no_pantype:
				{
					// PROCESS VOLUME FADEOUT
					
					if (ufchan.keyoff != 0)
					{
						ufchan.fadeoutvol -= donote_iptr.VOLfade;
						if (ufchan.fadeoutvol >= 0)
						{
							goto donote_fadevol_ok;
						}
						ufchan.fadeoutvol = 0;
					}
				}

			donote_fadevol_ok:
				{
					// PROCESS TICK 0 EFFECTS
					
					uint effec = noteptr[ni].effect;
					effec--; // skip FMUSIC_XM_ARPEGGIO

					//hacks for 9,11,13,21,25
					_mod.S1.donote_jumpflag = (byte)donote_jumpflag;
					_mod.S1.donote_sptr = donote_sptr;
					_mod.S1.donote_iptr = donote_iptr;

					//hacks for 14.13
					_mod.S1.S2_C13.donote_oldfreq = (uint)donote_oldfreq;
					_mod.S1.S2_C13.donote_oldpan = (uint)donote_oldpan;
					_mod.S1.S2_C13.donote_currtick = donote_currtick;

					if (effec <= 0x20)
						S1_TBL[effec](ufchan, noteptr[ni].eparam);
					
					DoFlags(ufchan, donote_iptr, donote_sptr);
					
				}

				//noteptr++;
				//ufchan++;
				ci++;
				ni++;
			} while (--numchannels !=0);

			
			return patternptr;
		}


		private static void S1_r(uF_CHAN ufchan, int dl) { }

		private delegate void S1_delegate(uF_CHAN channel, int param);

		private static readonly S1_delegate[] S1_TBL =
		{
			S1_C1,
			S1_C1,
			S1_C3,
			S1_C4,
			S1_C5,
			S1_C6,
			S1_C7,
			S1_C8,
			S1_C9,
			S1_C10,
			S1_C11,
			S1_C12,
			S1_C13,
			S1_C14,
			S1_C15,
			S1_C16,
			S1_C17,
			S1_r, // unassigned effect ordinal[18]
			S1_r, // unassigned effect ordinal[19]
			S1_r, // skip FMUSIC_XM_KEYOFF
			S1_C21,
			S1_r, // unassigned effect ordinal[22]
			S1_r, // unassigned effect ordinal[23]
			S1_r, // unassigned effect ordinal[24]
			S1_C25,
			S1_r, // unassigned effect ordinal[26]
			S1_C27,
			S1_r, // unassigned effect ordinal[28]
			S1_C29,
			S1_r, // unassigned effect ordinal[30]
			S1_r, // unassigned effect ordinal[31]
			S1_r, // unassigned effect ordinal[32]
			S1_C33
		};

		private static void S1_C1(uF_CHAN channel, int param)
		{
			if (param!=0)
				channel.portaupdown = (sbyte)param;
		}

		private static void S1_C3(uF_CHAN channel, int param)
		{
			if (param!=0)
				channel.portaspeed = (byte)param;

			channel.notectrl &= NOT_FMUSIC_TRIGGER_OR_FRQ;
			channel.portatarget = channel.period;
		}

		private static void S1_C5(uF_CHAN channel, int param)
		{
			if (param!=0)
				channel.volslide = (byte)param;

			channel.notectrl &= NOT_FMUSIC_TRIGGER_OR_FRQ;
			channel.portatarget = channel.period;
		}

		private static void S1_C4(uF_CHAN channel, int param)
		{
			int ebx = param & 0xf;
			param >>= 4;
			if (param == 0)
				goto donote_vib_x_ok;

			channel.vibspeed = (byte)param;

		donote_vib_x_ok:
			if (ebx == 0)
				goto donote_vib_y_ok;

			channel.vibdepth = (sbyte)ebx;

		donote_vib_y_ok:

			Vibrato(channel);
		}

		private static void S1_C6(uF_CHAN channel, int param)
		{
			if (param!=0)
				channel.volslide = (byte)param;
			Vibrato(channel);
		}

		private static void S1_C7(uF_CHAN channel, int param)
		{
			int ebx = param & 0xf;
			param >>= 4;
			if (param == 0)
				goto donote_trem_x_ok;

			channel.tremolospeed = (byte)param;

		donote_trem_x_ok:
			if (ebx != 0)
				channel.tremolodepth = (sbyte)ebx;

			//donote_trem_y_ok:
		}

		private static void S1_C9(uF_CHAN ufchan, int param)
		{
			int eax;
			uF_SAMP sample;

			param <<= 8;
			if (param == 0)
				goto donote_soffset_ok;

			ufchan.sampleoffset = (uint)param;

		donote_soffset_ok:

			sample = _mod.S1.donote_sptr;
			param = (int)sample.loopstart;
			param += (int)sample.looplen;
			eax = (int)ufchan.sampleoffset;

			FSOUND_CHANNEL channel = ufchan.cptr;
			if (eax < param)
				goto donote_set_offset;

			eax = 0;
			ufchan.notectrl &= NOT_FMUSIC_TRIGGER;
			channel.mixpos = 0;
			channel.mixposlo = 0;

		donote_set_offset:
			channel.fsampleoffset = (uint)eax;
		}

		private static void S1_C10(uF_CHAN channel, int param)
		{
			if (param!=0)
				channel.volslide = (byte)param;
		}

		private static void S1_C11(uF_CHAN channel, int param)
		{
			uF_MOD module = _mod.module;

			module.nextrow = 0;
			module.nextorder = param;

			_mod.S1.donote_jumpflag++;

			//donote_set_nextord:
			if (module.nextorder >= module.numorders)
				module.nextorder = 0;

			//donote_nextorder_ok:
		}

		private static void S1_C13(uF_CHAN channel, int param)
		{
			int ecx;
			int ebx = param & 0xf;

			param >>= 4;
			param = param * 5;
			ecx = ebx + param * 2;// paramx * 10 + paramy (it's BCD)

			uF_MOD module =_mod.module;
			module.nextrow = ecx;

			if (_mod.S1.donote_jumpflag != 0)
				return;

			module.nextorder = module.order + 1;

			//donote_set_nextord:
			if (module.nextorder >= module.numorders)
				module.nextorder = 0;

			//donote_nextorder_ok:;
		}

		private static void S1_C15(uF_CHAN channel, int param)
		{
			if (param >= 0x20)
				goto donote_setbpm;

			_mod.module.speed = (uint)param;
			return;

		donote_setbpm:;
			SetBPM(param);
		}

		private static void S1_C17(uF_CHAN channel, int param)
		{
			if (param!=0)
				_mod.module.globalvsl = (byte)param;
		}

		private static void S1_C21(uF_CHAN channel, int param)
		{
			uF_INST instrument;
			int ecx;
			int eax;
			int edx = param;
			int donote_currtick;
			int numpoints;
			short[] volpts;

			instrument = _mod.S1.donote_iptr;
			volpts = instrument.VOLPoints.Select(x=>(short)x).ToArray();

			if ((instrument.VOLtype & 1)==0)
				return;

			// Search and reinterpolate new envelope position
			numpoints = instrument.VOLnumpoints;
			eax = 0;
			if (param <= volpts[0+2])
				goto donote_env_endwhile;
			donote_envwhile:
			if (eax == numpoints) //if (currpos == iptr.VOLnumpoints) break;
				goto donote_env_endwhile;
			eax++;
			if (param > volpts[eax * 2+2]) //if (current.eparam > iptr.VOLPoints[(currpos + 1) << 1]) break;
				goto donote_envwhile;
			donote_env_endwhile:

			channel.envvol.pos = (uint)eax;
			// if it is at the last position, abort the envelope and continue last volume
			numpoints--;
			channel.envvolstopped = (byte)((eax >= numpoints)?1:0);
			if (eax < numpoints)
				goto donote_env_continue;
			eax = volpts[numpoints * 2 - 1+2];
			channel.envvolfrac.val = eax;// cptr.envvol = iptr.VOLPoints[((iptr.VOLnumpoints - 1) << 1) + 1];
										 //donote_envelope_r:
			return;

		donote_env_continue:
			channel.envvol.tick = (uint)param;
			ecx = volpts[eax * 2 - 2+2]; //get tick at this point + VOL at this point;
			edx = ecx;
			ecx &= 0x0000ffff;
			donote_currtick = ecx;
			ecx = volpts[eax * 2+2]; //get tick at next point + VOL at next point
			eax = ecx;
			ecx &= 0x0000ffff;
			edx = 0;
			// interpolate 2 points to find delta step;
			ecx -= donote_currtick;
			int efx = edx;
			if (ecx == 0)
				goto donote_no_tickdiff;
			eax = 0;
			eax -= edx;
			long tmp = eax;
			tmp /= ecx;
			ecx = eax;

		donote_no_tickdiff:
			channel.envvol.delta = (uint)ecx;
			eax = (int)channel.envvol.tick;
			eax -= donote_currtick;
			eax *= ecx;
			edx = efx;
			eax += edx;
			channel.envvolfrac.frac = eax;
			eax >>= 16;
			channel.envvolfrac.val = eax & 0xffff;
			channel.envvol.pos++;
		}

		private static void S1_C25(uF_CHAN channel, int param)
		{
			if (param!=0)
			{
				channel.panslide = (byte)param;
				channel.notectrl |= FMUSIC_PAN;
			}
		}

		private static void S1_C27(uF_CHAN channel, int param)
		{
			if (param!=0)
				channel.retrigx = (byte)(((param >> 4) & 0xf) | ((param & 0xf) << 4));
		}

		private static void S1_C29(uF_CHAN channel, int param)
		{
			if (param!=0)
				channel.tremoron = (byte)(((param >> 4) & 0xf) | ((param & 0xf) << 4));

			Tremor(channel);
		}

		private static void S1_C33(uF_CHAN channel, int param)
		{
			int eax;
			int bl = param & 0xf;
			param >>= 4;
			param--;
			if (param != 0)
				goto donote_paramx_n1;

			if (bl == 0)
				goto donote_paramy_z1;

			channel.xtraportaup = (byte)bl;

		donote_paramy_z1:
			eax = channel.xtraportaup;
			channel.freq -= (uint)eax;

		donote_paramx_n1:
			param--;
			if (param != 0)
				goto donote_paramx_n2;

			if (bl == 0)
				goto donote_paramy_z2;

			channel.xtraportadown = (byte)bl;

		donote_paramy_z2:
			eax = channel.xtraportadown;
			channel.freq += (uint)eax;

		donote_paramx_n2:;
		}

		private static void S2_r(uF_CHAN esi, int edx) { }

		private delegate void S2_delegate(uF_CHAN channel, int param);

		private static void S1_C14(uF_CHAN channel, int p)
		{
			uint param = (uint)p;
			param >>= 4;
			param--; // skip FMUSIC_XM_SETFILTER
			if (param > 13)
				return;

			//donote_do_special:
			S2_TBL[param](channel, p & 0xf);
		}

		private static readonly S2_delegate[] S2_TBL =
		{
			S2_C1,
			S2_C2,
			S2_r,// skip FMUSIC_XM_SETGLISSANDO,
			S2_C4,
			S2_C5,
			S2_C6,
			S2_C7,
			S2_C8,
			S2_r,// skip FMUSIC_XM_RETRIG,
			S2_C10,
			S2_C11,
			S2_r,// skip FMUSIC_XM_NOTECUT,
			S2_C13,
			S2_C14,
		};

		private static void S2_C1(uF_CHAN channel, int param)
		{
			int fineportaup;
			if (param!=0)
				channel.fineportaup = (byte)param;

			fineportaup = channel.fineportaup;
			fineportaup <<= 2;
			channel.freq -= (uint)fineportaup;
		}

		private static void S2_C2(uF_CHAN channel, int param)
		{
			int fineportadown;
			if (param!=0)
				channel.fineportadown = (byte)param;

			fineportadown = channel.fineportadown;
			fineportadown <<= 2;
			channel.freq += (uint)fineportadown;
		}

		private static void S2_C4(uF_CHAN channel, int param)
		{
			channel.wavecontrol &= 0xF0;
			channel.wavecontrol |= (byte)param;
		}

		private static void S2_C5(uF_CHAN channel, int param)
		{
			_mod.S1.donote_sptr.finetune = (sbyte)param;
		}

		private static void S2_C6(uF_CHAN channel, int param)
		{
			int patloopno;
			if (param == 0)
			{
				channel.patlooprow = _mod.module.row;
				return;
			}

			patloopno = (int)channel.patloopno;
			patloopno--;
			if (patloopno >= 0)
				goto donote_patloopno_ok;

			patloopno = param;

		donote_patloopno_ok:
			channel.patloopno = (uint)patloopno;
			if (patloopno == 0)
				goto donote_patloopno_end;

			_mod.module.nextrow = channel.patlooprow;

		donote_patloopno_end:;
		}

		private static void S2_C7(uF_CHAN channel, int param)
		{
			channel.wavecontrol &= 0x0F;
			param <<= 4;
			channel.wavecontrol = (byte)param;
		}

		private static void S2_C8(uF_CHAN channel, int param)
		{
			param <<= 4;
			channel.pan = (uint)param;
			channel.notectrl |= FMUSIC_PAN;
		}

		private static void S2_C10(uF_CHAN channel, int param)
		{
			S2_C11(channel, -param);
		}

		private static void S2_C11(uF_CHAN channel, int param)
		{
			if (param!=0)
				channel.finevslup = (sbyte)param;

			channel.volume -= channel.finevslup;
		}

		private static void S2_C13(uF_CHAN channel, int param)
		{
			channel.freq = _mod.S1.S2_C13.donote_oldfreq;
			channel.pan = _mod.S1.S2_C13.donote_oldpan;
			channel.volume = _mod.S1.S2_C13.donote_currtick;
			channel.notectrl = 0;
		}

		private static void S1_C12(uF_CHAN channel, int param)
		{
			channel.volume = param;
		}

		private static void S2_C14(uF_CHAN esi, int param)
		{
			uF_MOD module = _mod.module;
			module.patterndelay = (uint)(param * module.speed);
		}

		private delegate void S3_delegate(uF_CHAN channel, uint x, uint y);

		private static void DoEffs(uF_MOD module)
		{
			uint eax;
			uF_CHAN channel;
			uF_NOTE doeff_current;
			uF_SAMP sample = null;
			// Point our note pointer to the correct pattern buffer, and to the
			// correct offset in this buffer indicated by row andnumber of channels

			uint pattern = module.orderlist[module.order];
			//doeff_current = module.pattern[pattern].data;
			if (module.pattern[pattern].data == null)
				return;

			//doeff_current += module.row * module.numchannels;
			int di = (int)(module.row * module.numchannels);

			int numchannels = (int)module.numchannels;
			int ci = 0;
			//doeff_for_channels:
			while (numchannels-->0)
			{
				doeff_current = module.pattern[pattern].data[di];
				channel = module.uFMOD_Ch[ci];
				uint key;

				uF_INST instrument = _mod.module.instrument[channel.inst];

				key = instrument.keymap[channel.note];

				if (key < 0x10)
					sample = instrument.sample[key];

				if (sample == null)
					sample = _mod.DummySamp;

				//doeff_valid_sptr:
				channel.voldelta = 0;
				channel.freqdelta = 0;
				channel.notectrl = 0;

				if ((instrument.VOLtype & 1) != 0)
					Envelope(channel, instrument, FMUSIC_VOLUME);

				if ((instrument.PANtype & 1) != 0)
					Envelope(channel, instrument, FMUSIC_PAN);

				// PROCESS VOLUME FADEOUT
				if (channel.keyoff != 0)
				{
					eax = instrument.VOLfade;
					channel.fadeoutvol -= (int)eax;
					if (channel.fadeoutvol < 0)
						channel.fadeoutvol = 0;
					channel.notectrl |= FMUSIC_VOLUME;
				}

				//doeff_fadevol_ok:
				{
					uint volbyte = doeff_current.uvolume;
					uint volctl;
					int volume;
					volctl = volbyte >> 4;
					volume = (int)(volbyte & 0xf);
					volctl -= 6;
					//todo refactor into switch
					if (volctl != 0)
					{
						volctl--;
						if (volctl != 0)
						{
							volctl -= 4;
							if (volctl != 0)
							{
								volctl--;
								volctl--;
								if (volctl != 0)
								{
									volctl--;
									if (volctl != 0)
									{
										volctl--;
										if (volctl != 0)
										{
											goto doeff_volbyte_end;
										}
										Portamento(channel);
										goto doeff_volbyte_end;
									}
									volume = -volume;
								}
								channel.pan -= (uint)volume;
								channel.notectrl |= FMUSIC_PAN;
								goto doeff_volbyte_end;
							}
							channel.vibdepth = (sbyte)volume;
							Vibrato(channel);
						}
					}
					else
					{
						volume = -volume;
					}
					channel.volume += volume;
					channel.notectrl |= FMUSIC_VOLUME;
				}
			doeff_volbyte_end:
				{
					uint eparam = doeff_current.eparam;
					uint xparam, yparam;
					uint effect = doeff_current.effect;
					yparam = eparam & 0xf;
					xparam = eparam >>= 4;
					//hack for S3_C0, S3_C14
					_mod.S3.doeff_current = doeff_current;
					_mod.S3.sample = sample;
					if (effect <= 0x1d)
						S3_TBL[effect](channel, xparam, yparam);
				}
				//doeff_s3_brk:
				DoFlags(channel, instrument, sample);

				//channel++;
				//doeff_current++;
				ci++;
				di++;
			}
			//doeff_R:;
		}

		private static void S3_r(uF_CHAN esi, uint ecx, uint ebx) { }

		private static void S3_Portamento(uF_CHAN channel, uint x, uint y) { Portamento(channel); }
		private static void S3_Vibrato(uF_CHAN channel, uint x, uint y) { Vibrato(channel); }

		private static void S3_Tremolo(uF_CHAN channel, uint x, uint y) { Tremolo(channel); }

		private static void S3_Tremor(uF_CHAN channel, uint x, uint y) { Tremor(channel); }

		private static readonly S3_delegate[] S3_TBL = {
			S3_C0,
			S3_C1,
			S3_C2,
			// cptr + 2 : ESI,
			S3_Portamento,
			// cptr + 2 : ESI,
			S3_Vibrato,
			S3_C5,
			S3_C6,
			// cptr + 2 : ESI,
			S3_Tremolo,
			S3_r, // skip FMUSIC_XM_SETPANPOSITION,
			S3_r, // skip FMUSIC_XM_SETSAMPLEOFFSET,
			S3_C10,
			S3_r, // skip FMUSIC_XM_PATTERNJUMP,
			S3_r, // skip FMUSIC_XM_SETVOLUME,
			S3_r, // skip FMUSIC_XM_PATTERNBREAK,
			S3_C14,
			S3_r, // skip FMUSIC_XM_SETSPEED,
			S3_r, // skip FMUSIC_XM_SETGLOBALVOLUME,
			S3_C17,
			S3_r, // unassigned effect ordinal[18],
			S3_r, // unassigned effect ordinal[19],
			S3_r, // skip FMUSIC_XM_KEYOFF,
			S3_r, // skip FMUSIC_XM_SETENVELOPEPOS,
			S3_r, // unassigned effect ordinal[22],
			S3_r, // unassigned effect ordinal[23],
			S3_r, // unassigned effect ordinal[24],
			S3_C25,
			S3_r, // unassigned effect ordinal[26],
			S3_C27,
			S3_r, // unassigned effect ordinal[28],
			//case FMUSIC_XM_TREMOR,
			//cptr: ESI,
			S3_Tremor,
		};

		private static void S3_C6(uF_CHAN channel, uint ecx_xparam, uint ebx_yparam)
		{
			Vibrato(channel);
			S3_C10(channel, ecx_xparam, ebx_yparam);
		}

		private static void S3_C5(uF_CHAN channel, uint ecx_xparam, uint ebx_yparam)
		{
			Portamento(channel);
			S3_C10(channel, 0, ebx_yparam);
		}

		private static void S3_C10(uF_CHAN channel, uint ecx_xparam, uint ebx_yparam)
		{
			int eax;
			int volslide = channel.volslide;
			eax = volslide;
			eax &= 0x0F;
			volslide >>= 4;
			if (volslide == 0)
				goto doeff_volslide_ok;

			// Slide up takes precedence over down
			eax = volslide;
			eax = -eax;

		doeff_volslide_ok:
			channel.volume -= eax;
			channel.notectrl |= FMUSIC_VOLUME;
		}

		private static void DoFreqSlide(uF_CHAN channel, int portaupdown)
		{
			int freq = (int)channel.freq;
			freq += portaupdown * 4;
			if (freq < 1)
				freq = 1;

			channel.freq = (uint)freq;
			channel.freqdelta = 0;
			channel.notectrl |= FMUSIC_FREQ;
		}

		private static void S3_C2(uF_CHAN channel, uint ecx_xparam, uint ebx_yparam)
		{
			DoFreqSlide(channel, channel.portaupdown);
		}

		private static void S3_C1(uF_CHAN channel, uint ecx_xparam, uint ebx_yparam)
		{
			DoFreqSlide(channel, -channel.portaupdown);
		}

		private static void S3_C0(uF_CHAN channel, uint ecx_xparam, uint ebx_yparam)
		{
			uF_NOTE edx = _mod.S3.doeff_current;

			if (edx.eparam <= (ecx_xparam >> 8))
				goto doeff_arpeggio_ok;

			int tick = (int)_mod.module.tick;
			int tickmod3;
			tickmod3 = tick % 3;
			tickmod3--;
			if (tickmod3 == 0)
				goto doeff_arpeg_c1;

			tickmod3--;
			if (tickmod3 != 0)
				goto doeffs_enable_freq;

			ecx_xparam = ebx_yparam;

		doeff_arpeg_c1:
			if ((_mod.module.flags & 1) == 0)
				goto doeffs_flagsn1;

			//doeffs_arpeggio_freqd:
			ecx_xparam <<= 6;
			channel.freqdelta = (int)ecx_xparam;
			goto doeffs_enable_freq;

		doeffs_flagsn1:
			{
				uF_SAMP sptr = _mod.S3.sample;
				int v0, v1;

				v0 = AmigaPeriod(sptr, channel.realnote);
				v1 = AmigaPeriod(sptr, channel.realnote + ecx_xparam);

				channel.freqdelta = v1 - v0;
			}
		doeffs_enable_freq:

			channel.notectrl |= FMUSIC_FREQ;

		doeff_arpeggio_ok:;
		}

		private delegate int S4_delegate(int vol);

		private static void S3_C27(uF_CHAN channel, uint ecx_xparam, uint ebx_yparam)
		{
			int retrigy, volume;

			retrigy = channel.retrigy;
			if (retrigy == 0)
				goto doeff_multiretrig_ok;

			long tick = _mod.module.tick % retrigy;
			if (tick != 0)
				goto doeff_multiretrig_ok;

			retrigy = channel.retrigx;
			retrigy &= 0x0F
				;
			channel.notectrl |= FMUSIC_TRIGGER;

			retrigy--;
			volume = channel.volume;

			if (retrigy < 0)
				goto doeff_multiretrig_ok;

			channel.notectrl |= FMUSIC_VOLUME;
			channel.volume = S4_TBL[retrigy](volume);

		doeff_multiretrig_ok:;
		}

		private static int S4_C1(int vol) { return vol - 1; }
		private static int S4_C2(int vol) { return vol - 2; }
		private static int S4_C3(int vol) { return vol - 4; }
		private static int S4_C4(int vol) { return vol - 8; }
		private static int S4_C5(int vol) { return vol - 16; }
		private static int S4_C6(int vol) { return (vol * 2) / 3; }
		private static int S4_C7(int vol) { return vol / 2; }
		private static int S4_r(int vol) { return vol; }
		private static int S4_C9(int vol) { return vol + 1; }
		private static int S4_C10(int vol) { return vol + 2; }
		private static int S4_C11(int vol) { return vol + 4; }
		private static int S4_C12(int vol) { return vol + 8; }
		private static int S4_C13(int vol) { return vol + 16; }
		private static int S4_C14(int vol) { return (vol * 3) / 2; }
		private static int S4_C15(int vol) { return vol * 2; }

		private static readonly S4_delegate[] S4_TBL = {
			S4_C1,
			S4_C2,
			S4_C3,
			S4_C4,
			S4_C5,
			S4_C6,
			S4_C7,
			S4_r,
			S4_C9,
			S4_C10,
			S4_C11,
			S4_C12,
			S4_C13,
			S4_C14,
			S4_C15
		};

		private static void S3_C25(uF_CHAN channel, uint ecx_xparam, uint ebx_yparam)
		{
			uint panparam, panslide;
			int pan;
			panslide = channel.panslide;
			panparam = panslide;
			panparam &= 0x0F;
			pan = (int)channel.pan;
			panslide >>= 4;

			// Slide right takes precedence over left;
			if (panslide == 0)
			{
				pan -= (int)panparam;
				if (pan >= 0)
					pan = (int)panslide;
			}
			else
			{
				//doeff_pan_slide_right:
				panslide += (uint)pan;
				pan = 255;
				if (panslide <= pan)
					pan = (int)panslide;
			}
			//doeff_panslide_ok:

			S1_C8(channel, pan);
		}

		private static void S1_C8(uF_CHAN channel, int edx)
		{
			channel.pan = (uint)edx;
			channel.notectrl |= FMUSIC_PAN;
		}

		private static void S3_C17(uF_CHAN channel, uint ecx_xparam, uint ebx_yparam)
		{
			uF_MOD ecx = _mod.module;
			int volume;
			int vsl;
			volume = (int)ecx.globalvolume;
			vsl = ecx.globalvsl;

			if ((vsl & 0xf0) != 0) // Slide up takes precedence over down;
				goto doeff_gvsl_slide_up;

			vsl &= 0x0F;
			vsl = -vsl;
			goto set_global_vol;

		doeff_gvsl_slide_up:
			vsl >>= 4;

		set_global_vol:
			volume += vsl;
			_mod.module.globalvolume = (uint)volume;
		}

		private static void S1_C16(uF_CHAN esi, int edx)
		{
			_mod.module.globalvolume = (uint)edx;
		}

		private static void S3_C14(uF_CHAN channel, uint ecx_xparam, uint ebx_yparam)
		{
			int eax;
			uint edx;

			//todo, refactor into switch
			if (ecx_xparam == (uint)FMUSIC_XMCOMMANDSSPECIAL.FMUSIC_XM_RETRIG)
				goto doeff_do_retrig;

			ecx_xparam -= (uint)FMUSIC_XMCOMMANDSSPECIAL.FMUSIC_XM_NOTECUT;
			if (ecx_xparam == 0)
				goto doeff_do_notecut;

			ecx_xparam--;
			if (ecx_xparam != 0)
				goto doeff_special_r;

			eax = 0;
			if (_mod.module.tick != ebx_yparam)
				goto doeff_notectrl_z;

			uF_SAMP sample = _mod.S3.sample;

			channel.fadeoutvol = 65536;
			edx = sample.defvol;
			ecx_xparam = (uint)(eax + 9);
			channel.volume = (int)edx;
			edx = (uint)channel.envvolfrac.val;

			if (ecx_xparam < edx)
				edx = 64;

			channel.envvolfrac.val = (int)edx;

			channel.envvol.tick = 0;
			channel.envvol.pos = 0;
			channel.envvol.delta = 0;
			channel.envpan.tick = 0;
			channel.envpan.pos = 0;
			channel.envpan.delta = 0;
			channel.ivibsweeppos = 0;
			channel.ivibpos = 0;
			channel.keyoff = 0;
			channel.envvolstopped = 0;
			channel.envpanstopped = 0;

			// Retrigger tremolo and vibrato waveforms;
			ecx_xparam = channel.wavecontrol;
			if (ecx_xparam >= 0x4F)
				goto z2_tremolopos_ok;
			channel.tremolopos = 0;

		z2_tremolopos_ok:
			if ((ecx_xparam & 0x0C)!=0)
				goto z2_vibpos_ok;
			channel.vibpos = 0;

		z2_vibpos_ok:
			eax = (int)channel.period;
			channel.notectrl |= FMUSIC_VOL_OR_FREQ_OR_TR;
			channel.freq = (uint)eax;

			{
				uF_NOTE beax = _mod.S3.doeff_current;
				VolByte(channel, beax.uvolume);
			}

			return;

		doeff_notectrl_z:
			channel.notectrl = (byte)eax;

		doeff_special_r:
			return;

		doeff_do_notecut:
			if (_mod.module.tick != ebx_yparam)
				goto doeff_notecut_ok;

			channel.volume = 0;
			channel.notectrl |= FMUSIC_VOLUME;

		doeff_notecut_ok:
			return;

		doeff_do_retrig:
			if (ebx_yparam == 0)
				goto doeff_retrig_ok;

			edx = _mod.module.tick % ebx_yparam;

			if (edx != 0)
				goto doeff_retrig_ok;
			channel.notectrl |= FMUSIC_VOL_OR_FREQ_OR_TR;

		doeff_retrig_ok:;
		}

		private static void DoFlags(uF_CHAN ufchan, uF_INST flags_iptr, uF_SAMP flags_sptr)
		{
			uint tmp_3;
			uint tmp_4;
			uint tmp_5;
			uint ecx;
			int edx, eax;
			uF_INST instrument;
			uint ivibposptr;

			// THIS GETS ADDED TO PREVIOUS FREQDELTAS
			instrument = flags_iptr;
			eax = 0;
			edx = instrument.VIBtype;
			ivibposptr = ufchan.ivibpos;

			edx--;
			if (edx < 0)
				goto ivib_case_0;

			eax = 0x80;
			if (edx == 0)
				goto ivib_case_1;

			edx--;
			if (edx == 0)
				goto ivib_case_2;

			edx = 0x80;
			edx -= (int)ivibposptr;
			
			trim:
			
			
				eax -= edx;
				eax >>= 1;
				// delta = (128 - ((384 - cptr.ivibpos) & 0xFF)) >> 1 (case 3)
				// delta = (128 - ((cptr.ivibpos + 128) & 0xFF)) >> 1 (case 2)
				goto ivib_end_switch;
			ivib_case_2:
				edx = (int)ivibposptr;
				edx -= eax;
				goto trim;

		ivib_case_1:
			edx -= edx;//todo sbb
			edx &= eax;
			eax = edx - 0x40; // delta = +/- 64
			goto ivib_end_switch;

		ivib_case_0:
			ecx = ivibposptr;
			edx = (int)ivibposptr;
			ecx &= 0x7f;
			eax += (int)(-1 + ((ecx < 65)?1:0));
			ecx ^= (uint)eax;
			eax &= 0x81;
			ecx += (uint)eax;
			int B = (edx & 128)!=0?1:0;
			edx >>= 8;
			//bug compatibility with ufmod.asm 1.25.2a
			//Sometimes this reads offset 0x40 (when ufchan.ivibpos == 0xC0)
			//reading 0xAB which is the first byte of constant f0_0833
			//assert(ecx < 0x41);
			eax = sin64[ecx];
			edx -= edx + B;
			eax ^= edx;
			eax -= edx;

		ivib_end_switch:

			

			edx = instrument.iVIBdepth;
			edx *= eax;
			eax = instrument.VIBsweep;
			if (eax != 0)
			{
				int edi = eax;
				eax = (int)ufchan.ivibsweeppos;
				long tmp = (long)eax * edx;
				eax = (int)(tmp / edi);
				edx = (int)(tmp % edi);
				tmp_3 = (uint)eax;
				eax = edx;
				edx = (int)tmp_3;
				tmp_3 = (uint)eax;
				eax = edi;
				edi = (int)tmp_3;
			}

			//sweep_ok:
			edx >>= 6;
			ufchan.freqdelta += edx;
			edx = (int)ufchan.ivibsweeppos;
			edx++;
			if (edx > eax)
			{
				tmp_3 = (uint)eax;
				eax = edx;
				edx = (int)tmp_3;
			}

			//sweeppos_ok:
			
			{
				eax = instrument.VIBrate;
				eax += (int)ivibposptr;
				ufchan.ivibsweeppos = (uint)edx;
				ufchan.notectrl |= FMUSIC_FREQ;
				if ((eax & 0xff00)!=0)//todo
					eax -= 256;
				ufchan.ivibpos = (uint)eax;

				FSOUND_CHANNEL channel = ufchan.cptr;

				if ((ufchan.notectrl & FMUSIC_TRIGGER) != 0)
				{
					if (channel.fsptr != null)
					{
						FSOUND_CHANNEL altchannel=null;
						//uintptr_t ecx;

						//// Swap between channels to avoid sounds cutting each other off and causing a click
						//ecx = (uintptr_t)channel;
						//ecx -= (uintptr_t)_mod.module.Channels;
						////ecx is offset of channels

						////if we're on the odd numbered one, go to the even one, else go to the odd one
						//if ((ecx / FSOUND_CHANNEL_size) & 1)
						//	altchannel = channel - 1;
						//else
						//	altchannel = channel + 1;

						for (int i = 0; i < _mod.module.Channels.Length; i++)
						{
							if (channel == _mod.module.Channels[i])
							{
								altchannel = _mod.module.Channels[i^1];
								break;
							}
						}

						// Copy the whole channel except its trailing data
						ufchan.cptr = altchannel;

						//ecx = offsetof(FSOUND_CHANNEL, fsptr);//  (FSOUND_CHANNEL_size - 20)/4
						//ufmemcpy(altchannel, channel, ecx);

						altchannel.actualvolume = channel.actualvolume;
						altchannel.actualpan = channel.actualpan;
						altchannel.fsampleoffset = channel.fsampleoffset;
						altchannel.leftvolume = channel.leftvolume;
						altchannel.rightvolume = channel.rightvolume;
						altchannel.mixpos = channel.mixpos;
						altchannel.speedlo = channel.speedlo;
						altchannel.speedhi = channel.speedhi;
						altchannel.ramp_LR_target = channel.ramp_LR_target;
						altchannel.ramp_leftspeed = channel.ramp_leftspeed;
						altchannel.ramp_rightspeed = channel.ramp_rightspeed;

						// This should cause the old channel to ramp out nicely
						channel.actualvolume = 0;
						channel.leftvolume = 0;
						channel.rightvolume = 0;
						channel = altchannel;
					}
					//no_swap:
					{
						channel.fsptr = flags_sptr;

						// START THE SOUND!

						channel.mixposlo = 0;
						channel.ramp_leftvolume = 0;
						channel.ramp_rightvolume = 0;
						channel.ramp_count = 0;
						channel.speeddir = 0;
						channel.mixpos = channel.fsampleoffset;
						channel.fsampleoffset = 0;
					}
				}
				//no_trig:

				{
					int edi;
					uint ecx2 = 255;

					if ((ufchan.notectrl & FMUSIC_VOLUME) == 0)
						goto no_volume;

					eax = ufchan.volume;
					eax += ufchan.voldelta;
					edx = eax;
					eax <<= 8;
					eax -= edx;
					long tmp = (long)eax * (int)_mod.module.globalvolume;
					eax = (int)tmp;
					edx = (int)(tmp >> 32);
					edx = ~edx;
					eax &= edx;
					if (eax > 0xff000)
						eax = 0xff000;

					//vol_upper_bound_ok:;
					tmp = (long)eax * ufchan.envvolfrac.val;
					tmp = tmp * ufchan.fadeoutvol;
					edx = (int)(tmp >> 32);
					edx >>= 3;
					eax = (int)channel.actualpan;
					channel.actualvolume = (uint)edx;

					edi = edx;
					tmp = (long)eax * edx;
					eax = (int)(tmp / ecx2);
					channel.leftvolume = (uint)eax;

					eax = (int)ecx2;
					eax -= (int)channel.actualpan;
					tmp = (long)eax * edi;
					eax = (int)(tmp / ecx2);
					channel.rightvolume = (uint)eax;
				no_volume:
					if ((ufchan.notectrl & FMUSIC_PAN) != 0)
					{
						edi = 0x80;
						eax = (int)ufchan.pan;
						eax -= edi;
						edx = (int)cdq((uint)eax);
						eax ^= edx;
						eax -= edx;
						edi -= eax;
						eax = ufchan.envpanfrac.val;
						edi >>= 5;
						eax -= 0x20;
						eax = eax * edi;
						eax += (int)ufchan.pan;
						edx = (int)cdq((uint)eax);
						edx = ~edx;
						eax &= edx;
						edi = (int)channel.actualvolume;
						edx = (int)ecx2;
						tmp_4 = (uint)eax;
						eax = edi;
						edi = (int)tmp_4;
						if (edi < ecx2)
						{
							long tmp3 = (long)eax * edi;
							eax = (int)(tmp3 / ecx2);
							edx = edi;
						}
						//pan_ae255:
						tmp_5 = (uint)eax;
						eax = edx;
						edx = (int)tmp_5;
						channel.actualpan = (uint)eax;
						channel.leftvolume = (uint)edx;
						eax ^= 0xff;
						long tmp2 = (long)eax * channel.actualvolume;
						eax = (int)(tmp2 / ecx2);
						channel.rightvolume = (uint)eax;
					}
					//no_pan:
					

					if ((ufchan.notectrl & FMUSIC_FREQ) != 0)
					{
						ecx2 = ufchan.freq;
						edx = 0;
						ecx2 += (uint)ufchan.freqdelta;
						eax = edx + 0x28;// f = 40 Hz
						if (ecx2 > 0)
						{
							if ((_mod.module.flags & 1) == 0)
							{
								eax = (int)(0xda7790 / ecx2);//14317456
							}
							else
							{
								//modflags_n1:;
								int tmp4;
								tmp4 = 0x1200;
								tmp4 -= (int)ufchan.freq;
								tmp4 += ufchan.freqdelta;

								/*
								__asm ("fild dword [tmp]");
								__asm ("fmul dword [f0_0013]");
								__asm ("fld st(0)");
								__asm ("frndint");
								__asm ("fsub st(1), st(0)");
								__asm ("fxch st(1)");
								__asm ("f2xm1");
								__asm ("fld1");
								__asm ("faddp st(1)");
								__asm ("fscale");
								__asm ("fstp st(1)");
								__asm ("fmul dword [f8363_0]");
								__asm ("fistp dword [tmp]");
								*/

								/*
								multiply by 0.00133
								separate fraction and int parts of number
								add the powers of 2 of each part
								multiply by 8363.0
								return (int)(f8363_0 * powf(2.0f, eax * f0_0013));
								*/
								//int ans = (int)(f8363_0 * powf(2.0f, (float)tmp * f0_0013));

								//tmp4 = (int)Math.Round(f8363_0 * Math.Pow(2.0, tmp4 * f0_0013));
								tmp4 = FreqTab.pow2_table_DoFlags(tmp4);

								eax = tmp4;
							}
						}
						
						//freq_bounds_ok:
						int tmp5;
						long tmp64;
						tmp5 = eax / FSOUND_MixRate;
						tmp64 = (long)(eax % FSOUND_MixRate) << 32;
						channel.speedhi = tmp5;

						eax = (int)(tmp64 / FSOUND_MixRate);
						channel.speedlo = (uint)eax;
						//no_freq:;
						
					}
				}
			}
			
		}

	}
}