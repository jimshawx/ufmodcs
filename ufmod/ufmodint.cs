﻿using System;
using System.IO;

namespace uFMOD
{


	/*
	; Sample type - contains info on sample
	uF_SAMP__length   EQU 0; sample length
	uF_SAMP_loopstart EQU 4; loop start
	uF_SAMP_looplen   EQU 8; loop length
	uF_SAMP_defvol    EQU 12; default volume
	uF_SAMP_finetune  EQU 13; finetune value from - 128 to 127
	uF_SAMP_bytes     EQU 14; type[b 0 - 1] : 0 - no loop
	;                1 - forward loop
	;                2 - bidirectional loop(aka ping - pong)
	; [b 4] : 0 - 8 - bit sample data
	;                1 - 16 - bit sample data
	uF_SAMP_defpan    EQU 15; default pan value from 0 to 255
	uF_SAMP_relative  EQU 16; relative note(signed value)
	uF_SAMP_Resved    EQU 17; reserved, known values : 00h - regular delta packed sample data
	;                         ADh - ModPlug 4 - bit ADPCM packed sample data
	uF_SAMP_loopmode  EQU 18
	uF_SAMP__align    EQU 19
	uF_SAMP_buff      EQU 20; sound data
	uF_SAMP_size      EQU 22
	*/
	public class uF_SAMP
	{
		public uint _length; // sample length
		public uint loopstart; // loop start
		public uint looplen; // loop length
		public byte defvol; // default volume
		public sbyte finetune; // finetune value from - 128 to 127
		public byte bytes;// type[b 0 - 1] : 0 - no loop
						  //                1 - forward loop
						  //                2 - bidirectional loop(aka ping - pong)
						  // [b 4] : 0 - 8 - bit sample data
						  //                1 - 16 - bit sample data
		public byte defpan; // default pan value from 0 to 255
		public sbyte relative; // relative note(signed value)
		public byte Resved; // reserved, known values : 00h - regular delta packed sample data
							//                         ADh - ModPlug 4 - bit ADPCM packed sample data
		public byte loopmode;
		public byte _align;
		//public ushort buff; // sound data marker
		public ushort[] buff = new ushort[0];
	}
	//const int uF_SAMP_size (offsetof(uF_SAMP, buff))


/*
; Channel type - contains information on a mixing channel
FSOUND_CHANNEL_actualvolume     EQU 0; volume
FSOUND_CHANNEL_actualpan        EQU 4; panning value
FSOUND_CHANNEL_fsampleoffset    EQU 8; sample offset(sample starts playing from here)
FSOUND_CHANNEL_leftvolume       EQU 12; adjusted volume for left channel(panning involved)
FSOUND_CHANNEL_rightvolume      EQU 16; adjusted volume for right channel(panning involved)
FSOUND_CHANNEL_mixpos           EQU 20; high part of 32:32 fractional position in sample
FSOUND_CHANNEL_speedlo          EQU 24; playback rate - low part fractional
FSOUND_CHANNEL_speedhi          EQU 28; playback rate - high part fractional
FSOUND_CHANNEL_ramp_LR_target   EQU 32
FSOUND_CHANNEL_ramp_leftspeed   EQU 36
FSOUND_CHANNEL_ramp_rightspeed  EQU 40
FSOUND_CHANNEL_fsptr            EQU 44; pointer to FSOUND_SAMPLE currently playing sample
FSOUND_CHANNEL_mixposlo         EQU 48; low part of 32:32 fractional position in sample
FSOUND_CHANNEL_ramp_leftvolume  EQU 52
FSOUND_CHANNEL_ramp_rightvolume EQU 56
FSOUND_CHANNEL_ramp_count       EQU 60
FSOUND_CHANNEL_speeddir         EQU 62; playback direction - forwards or backwards
FSOUND_CHANNEL_size             EQU 64
*/
public class FSOUND_CHANNEL
	{
		public uint actualvolume; // volume
		public uint actualpan; // panning value
		public uint fsampleoffset; // sample offset(sample starts playing from here)
		public uint leftvolume; // adjusted volume for left channel(panning involved)
		public uint rightvolume; // adjusted volume for right channel(panning involved)
		public uint mixpos; // high part of 32:32 fractional position in sample
		public uint speedlo; // playback rate - low part fractional
		public int speedhi; // playback rate - high part fractional
		public uint ramp_LR_target;
		public int ramp_leftspeed;
		public int ramp_rightspeed;
		public uF_SAMP fsptr; // pointer to FSOUND_SAMPLE currently playing sample
		public uint mixposlo; // low part of 32:32 fractional position in sample
		public uint ramp_leftvolume;
		public uint ramp_rightvolume;
		public ushort ramp_count;
		public ushort speeddir; // playback direction - 0 forwards or 1 backwards
	}


	/*
	; Single note type - contains info on 1 note in a pattern
	uF_NOTE_unote   EQU 0; note to play at(0 - 97) 97 = keyoff
	uF_NOTE_number  EQU 1; sample being played(0 - 128)
	uF_NOTE_uvolume EQU 2; volume column value(0 - 64)  255 = no volume
	uF_NOTE_effect  EQU 3; effect number(0 - 1Ah)
	uF_NOTE_eparam  EQU 4; effect parameter(0 - 255)
	uF_NOTE_size    EQU 5
	*/
	public class uF_NOTE
	{
		public byte unote; // note to play at(0 - 97) 97 = keyoff
		public byte number; // sample being played(0 - 128)
		public byte uvolume; // volume column value(0 - 64)  255 = no volume
		public byte effect; // effect number(0 - 1Ah)
		public byte eparam; // effect parameter(0 - 255)
	}


	/*
	uF_STATS_L_vol   EQU 0; L channel RMS volume
	uF_STATS_R_vol   EQU 2; R channel RMS volume
	uF_STATS_s_row   EQU 4
	uF_STATS_s_order EQU 6
	uF_STATS_size    EQU 8
	*/
	public class uF_STATS
	{
		public ushort L_vol; // L channel RMS volume
		public ushort R_vol; // R channel RMS volume
		public ushort s_row;
		public ushort s_order;

		public void Clear()
		{
			L_vol = R_vol = s_row = s_order = 0;
		}
	}

	/*
	; Pattern data type
	uF_PAT_rows        EQU 0
	uF_PAT_patternsize EQU 2
	uF_PAT_data        EQU 4; pointer to FMUSIC_NOTE
	uF_PAT_size        EQU 8
	*/
	public class uF_PAT
	{
		public short rows;
		public ushort patternsize;
		public uF_NOTE[] data; // pointer to FMUSIC_NOTE
	}

	/*
	; Multi sample extended instrument
	uF_INST_sample       EQU 0; 16 pointers to FSOUND_SAMPLE per instrument
	uF_INST_keymap       EQU 64; sample keymap assignments
	uF_INST_VOLPoints    EQU 160; volume envelope points
	uF_INST_PANPoints    EQU 208; panning envelope points
	uF_INST_VOLnumpoints EQU 256; number of volume envelope points
	uF_INST_PANnumpoints EQU 257; number of panning envelope points
	uF_INST_VOLsustain   EQU 258; volume sustain point
	uF_INST_VOLLoopStart EQU 259; volume envelope loop start
	uF_INST_VOLLoopEnd   EQU 260; volume envelope loop end
	uF_INST_PANsustain   EQU 261; panning sustain point
	uF_INST_PANLoopStart EQU 262; panning envelope loop start
	uF_INST_PANLoopEnd   EQU 263; panning envelope loop end
	uF_INST_VOLtype      EQU 264; type of envelope, bit 0:On 1 : Sustain 2 : Loop
	uF_INST_PANtype      EQU 265; type of envelope, bit 0:On 1 : Sustain 2 : Loop
	uF_INST_VIBtype      EQU 266; instrument vibrato type
	uF_INST_VIBsweep     EQU 267; time it takes for vibrato to fully kick in
	uF_INST_iVIBdepth    EQU 268; depth of vibrato
	uF_INST_VIBrate      EQU 269; rate of vibrato
	uF_INST_VOLfade      EQU 270; fade out value
	uF_INST_size         EQU 272
	*/
	public class uF_INST
	{
		public uF_SAMP[] sample = new uF_SAMP[16]; // 16 pointers to FSOUND_SAMPLE per instrument
		public byte[] keymap = new byte[96]; // sample keymap assignments
		public ushort[] VOLPoints = new ushort[24]; // volume envelope points
		public ushort[] PANPoints = new ushort[24]; // panning envelope points
		public byte VOLnumpoints; // number of volume envelope points
		public byte PANnumpoints; // number of panning envelope points
		public byte VOLsustain; // volume sustain point
		public byte VOLLoopStart; // volume envelope loop start
		public byte VOLLoopEnd; // volume envelope loop end
		public byte PANsustain; // panning sustain point
		public byte PANLoopStart; // panning envelope loop start
		public byte PANLoopEnd; // panning envelope loop end
		public byte VOLtype; // type of envelope, bit 0:On 1 : Sustain 2 : Loop
		public byte PANtype; // type of envelope, bit 0:On 1 : Sustain 2 : Loop
		public byte VIBtype; // instrument vibrato type
		public byte VIBsweep; // time it takes for vibrato to fully kick in
		public byte iVIBdepth; // depth of vibrato
		public byte VIBrate; // rate of vibrato
		public ushort VOLfade; // fade out value
	}

	/*
	; Channel type - contains information on a mod channel
	uF_CHAN_note          EQU 0; last note set in channel
	uF_CHAN_samp          EQU 1; last sample set in channel
	uF_CHAN_notectrl      EQU 2; flags for DoFlags proc
	uF_CHAN_inst          EQU 3; last instrument set in channel
	uF_CHAN_cptr          EQU 4; pointer to FSOUND_CHANNEL system mixing channel
	uF_CHAN_freq          EQU 8; current mod frequency period for this channel
	uF_CHAN_volume        EQU 12; current mod volume for this channel
	uF_CHAN_voldelta      EQU 16; delta for volume commands : tremolo / tremor, etc
	uF_CHAN_freqdelta     EQU 20; delta for frequency commands : vibrato / arpeggio, etc
	uF_CHAN_pan           EQU 24; current mod pan for this channel
	uF_CHAN_envvoltick    EQU 28; tick counter for envelope position
	uF_CHAN_envvolpos     EQU 32; envelope position
	uF_CHAN_envvoldelta   EQU 36; delta step between points
	uF_CHAN_envpantick    EQU 40; tick counter for envelope position
	uF_CHAN_envpanpos     EQU 44; envelope position
	uF_CHAN_envpandelta   EQU 48; delta step between points
	uF_CHAN_ivibsweeppos  EQU 52; instrument vibrato sweep position
	uF_CHAN_ivibpos       EQU 56; instrument vibrato position
	uF_CHAN_keyoff        EQU 60; flag whether keyoff has been hit or not
	uF_CHAN_envvolstopped EQU 62; flag to say whether envelope has finished or not
	uF_CHAN_envpanstopped EQU 63; flag to say whether envelope has finished or not
	uF_CHAN_envvolfrac    EQU 64; fractional interpolated envelope volume
	uF_CHAN_envvol        EQU 68; final interpolated envelope volume
	uF_CHAN_fadeoutvol    EQU 72; volume fade out
	uF_CHAN_envpanfrac    EQU 76; fractional interpolated envelope pan
	uF_CHAN_envpan        EQU 80; final interpolated envelope pan
	uF_CHAN_period        EQU 84; last period set in channel
	uF_CHAN_sampleoffset  EQU 88; sample offset for this channel in SAMPLES
	uF_CHAN_portatarget   EQU 92; note to porta to
	uF_CHAN_patloopno     EQU 96; pattern loop variables for effect E6x
	uF_CHAN_patlooprow    EQU 100
	uF_CHAN_realnote      EQU 104; last realnote set in channel
	uF_CHAN_recenteffect  EQU 105; previous row's effect. used to correct tremolo volume
	uF_CHAN_portaupdown   EQU 106; last porta up / down value
	; uF_CHAN_unused       EQU 107
	uF_CHAN_xtraportadown EQU 108; last porta down value
	uF_CHAN_xtraportaup   EQU 109; last porta up value
	uF_CHAN_volslide      EQU 110; last volume slide value
	uF_CHAN_panslide      EQU 111; pan slide parameter
	uF_CHAN_retrigx       EQU 112; last retrig volume slide used
	uF_CHAN_retrigy       EQU 113; last retrig tick count used
	uF_CHAN_portaspeed    EQU 114; porta speed
	uF_CHAN_vibpos        EQU 115; vibrato position
	uF_CHAN_vibspeed      EQU 116; vibrato speed
	uF_CHAN_vibdepth      EQU 117; vibrato depth
	uF_CHAN_tremolopos    EQU 118; tremolo position
	uF_CHAN_tremolospeed  EQU 119; tremolo speed
	uF_CHAN_tremolodepth  EQU 120; tremolo depth
	uF_CHAN_tremorpos     EQU 121; tremor position
	uF_CHAN_tremoron      EQU 122; remembered parameters for tremor
	uF_CHAN_tremoroff     EQU 123; remembered parameters for tremor
	uF_CHAN_wavecontrol   EQU 124; waveform type for vibrato andtremolo(4bits each)
	uF_CHAN_finevslup     EQU 125; parameter for fine volume slide down
	uF_CHAN_fineportaup   EQU 126; parameter for fine porta slide up
	uF_CHAN_fineportadown EQU 127; parameter for fine porta slide down
	uF_CHAN_size          EQU 128
	*/
	public class uF_VOLPAN
	{
		public uint tick;
		public uint pos;
		public uint delta;
	}

	public class uF_VOLPANFRAC
	{
		public int frac;
		public int val;
	}

	public class uF_CHAN
	{
		public byte note; // last note set in channel
		public byte samp; // last sample set in channel
		public byte notectrl; // flags for DoFlags proc
		public byte inst; // last instrument set in channel
		public FSOUND_CHANNEL cptr; // pointer to FSOUND_CHANNEL system mixing channel
		public uint freq; // current mod frequency period for this channel
		public int volume; // current mod volume for this channel
		public int voldelta; // delta for volume commands : tremolo / tremor, etc
		public int freqdelta; // delta for frequency commands : vibrato / arpeggio, etc
		public uint pan; // current mod pan for this channel
		/*
		uint envvoltick; // tick counter for envelope position
		uint envvolpos; // envelope position
		uint envvoldelta; // delta step between points
		uint envpantick; // tick counter for envelope position
		uint envpanpos; // envelope position
		uint envpandelta; // delta step between points
		*/
		public uF_VOLPAN envvol= new uF_VOLPAN();
		public uF_VOLPAN envpan=new uF_VOLPAN();
		public uint ivibsweeppos; // instrument vibrato sweep position
		public uint ivibpos; // instrument vibrato position
		public ushort keyoff; // flag whether keyoff has been hit or not
		public byte envvolstopped; // flag to say whether envelope has finished or not
		public byte envpanstopped; // flag to say whether envelope has finished or not
		/*
		uint envvolfrac; // fractional interpolated envelope volume
		uint envvol; // final interpolated envelope volume
		*/
		public uF_VOLPANFRAC envvolfrac = new uF_VOLPANFRAC();
		public int fadeoutvol; // volume fade out
		/*
		uint envpanfrac; // fractional interpolated envelope pan
		uint envpan; // final interpolated envelope pan
		*/
		public uF_VOLPANFRAC envpanfrac = new uF_VOLPANFRAC();
		public uint period; // last period set in channel
		public uint sampleoffset; // sample offset for this channel in SAMPLES
		public uint portatarget; // note to porta to
		public uint patloopno; // pattern loop variables for effect E6x
		public int patlooprow;
		public byte realnote; // last realnote set in channel
		public byte recenteffect; // previous row's effect. used to correct tremolo volume
		public sbyte portaupdown; // last porta up / down value
		public byte unused;
		public byte xtraportadown; // last porta down value
		public byte xtraportaup; // last porta up value
		public byte volslide; // last volume slide value
		public byte panslide; // pan slide parameter
		public byte retrigx; // last retrig volume slide used
		public byte retrigy; // last retrig tick count used
		public byte portaspeed; // porta speed
		public byte vibpos; // vibrato position
		public byte vibspeed; // vibrato speed
		public sbyte vibdepth; // vibrato depth
		public byte tremolopos; // tremolo position
		public byte tremolospeed; // tremolo speed
		public sbyte tremolodepth; // tremolo depth
		public byte tremorpos; // tremor position
		public byte tremoron; // remembered parameters for tremor
		public byte tremoroff; // remembered parameters for tremor
		public byte wavecontrol; // waveform type for vibrato and tremolo(4bits each)
		public sbyte finevslup; // parameter for fine volume slide down
		public byte fineportaup; // parameter for fine porta slide up
		public byte fineportadown; // parameter for fine porta slide down

		public IVibratoOrTremolo Vibrato => new VibratoOrTremolo(vibpos, vibspeed, vibdepth, (x)=>vibpos=x, () => vibpos);
		public IVibratoOrTremolo Tremolo => new VibratoOrTremolo(tremolopos, tremolospeed, tremolodepth, (x)=>tremolopos=x, () => tremolopos);
	}

	public class VibratoOrTremolo:IVibratoOrTremolo
	{
		public VibratoOrTremolo(byte vibpos, byte vibspeed, sbyte vibdepth, Action<byte> setVibPos, Func<byte> getVibPos)
		{
			this.setVibPos = setVibPos;
			this.getVibPos = getVibPos;
			//this.vibpos = vibpos;
			this.vibspeed = vibspeed;
			this.vibdepth = vibdepth;
		}

		private readonly Action<byte> setVibPos;
		private readonly Func<byte> getVibPos;
		
		public byte vibpos 
		{
			get => getVibPos();
			set => setVibPos(value);
		}
		public byte vibspeed { get; }
		public sbyte vibdepth { get; }
	}

	public interface IVibratoOrTremolo 
	{
		byte vibpos { get; set; } // vibrato position
		byte vibspeed { get; } // vibrato speed
		sbyte vibdepth { get; } // vibrato depth
	}

	/*
	; Song type - contains info on song
	uF_MOD_pattern              EQU 0; pointer to FMUSIC_PATTERN array for this song
	uF_MOD_instrument           EQU 4; pointer to FMUSIC_INSTRUMENT array for this song
	uF_MOD_mixer_samplesleft    EQU 8
	uF_MOD_globalvolume         EQU 12; global mod volume
	uF_MOD_tick                 EQU 16; current mod tick
	uF_MOD_speed                EQU 20; speed of song in ticks per row
	uF_MOD_order                EQU 24; current song order position
	uF_MOD_row                  EQU 28; current row in pattern
	uF_MOD_patterndelay         EQU 32; pattern delay counter
	uF_MOD_nextorder            EQU 36; current song order position
	uF_MOD_nextrow              EQU 40; current row in pattern
	uF_MOD_unused1              EQU 44
	uF_MOD_numchannels          EQU 48; number of channels
	uF_MOD_Channels             EQU 52; channel pool
	uF_MOD_uFMOD_Ch             EQU 56; channel array for this song
	uF_MOD_mixer_samplespertick EQU 60
	uF_MOD_numorders            EQU 64; number of orders(song length)
	uF_MOD_restart              EQU 66; restart position
	uF_MOD_numchannels_xm       EQU 68
	uF_MOD_globalvsl            EQU 69; global mod volume
	uF_MOD_numpatternsmem       EQU 70; number of allocated patterns
	uF_MOD_numinsts             EQU 72; number of instruments
	uF_MOD_flags                EQU 74; flags such as linear frequency, format specific quirks, etc
	uF_MOD_defaultspeed         EQU 76
	uF_MOD_defaultbpm           EQU 78
	uF_MOD_orderlist            EQU 80; pattern playing order list
	uF_MOD_size                 EQU 336
	*/
	public class uF_MOD
	{
		public uF_PAT[] pattern; // pointer to FMUSIC_PATTERN array for this song 
		public uF_INST[] instrument; // pointer to FMUSIC_INSTRUMENT array for this song
		public uint mixer_samplesleft;
		public uint globalvolume; // global mod volume
		public uint tick; // current mod tick
		public uint speed; // speed of song in ticks per row
		public int order; // current song order position
		public int row; // current row in pattern
		public uint patterndelay; // pattern delay counter
		public int nextorder; // current song order position
		public int nextrow; // current row in pattern
		public uint unused1;
		public uint numchannels; // number of channels
		public FSOUND_CHANNEL[] Channels; // channel pool
		public uF_CHAN[] uFMOD_Ch; // channel array for this song

		//public union
		//{
		public uint mixer_samplespertick; //overlays XM module header size
		public uint header_size;
		//}

		public short numorders; // number of orders(song length)
		public ushort restart; // restart position
		public byte numchannels_xm;
		public byte globalvsl; // global mod volume // numchannels_xm is usually a word, this has been jammed in between since numchannels is <= 32
		public ushort numpatternsmem; // number of allocated patterns
		public ushort numinsts; // number of instruments
		public ushort flags; // flags such as linear frequency, format specific quirks, etc
		public ushort defaultspeed;
		public ushort defaultbpm;
		public byte[] orderlist = new byte[256]; // pattern playing order list
	}


	public enum FMUSIC_XMCOMMANDS
	{
		FMUSIC_XM_PORTAUP = 1,
		FMUSIC_XM_PORTADOWN = 2,
		FMUSIC_XM_PORTATO = 3,
		FMUSIC_XM_VIBRATO = 4,
		FMUSIC_XM_PORTATOVOLSLIDE = 5,
		FMUSIC_XM_VIBRATOVOLSLIDE = 6,
		FMUSIC_XM_TREMOLO = 7,
		FMUSIC_XM_SETPANPOSITION = 8,
		FMUSIC_XM_SETSAMPLEOFFSET = 9,
		FMUSIC_XM_VOLUMESLIDE = 10,
		FMUSIC_XM_PATTERNJUMP = 11,
		FMUSIC_XM_SETVOLUME = 12,
		FMUSIC_XM_PATTERNBREAK = 13,
		FMUSIC_XM_SPECIAL = 14,
		FMUSIC_XM_SETSPEED = 15,
		FMUSIC_XM_SETGLOBALVOLUME = 16,
		FMUSIC_XM_GLOBALVOLSLIDE = 17,
		FMUSIC_XM_KEYOFF = 20,
		FMUSIC_XM_PANSLIDE = 25,
		FMUSIC_XM_MULTIRETRIG = 27,
		FMUSIC_XM_TREMOR = 29,
		FMUSIC_XM_EXTRAFINEPORTA = 33
	};

	public enum FMUSIC_XMCOMMANDSSPECIAL
	{
		FMUSIC_XM_FINEPORTAUP = 1,
		FMUSIC_XM_FINEPORTADOWN = 2,
		FMUSIC_XM_SETGLISSANDO = 3,
		FMUSIC_XM_SETVIBRATOWAVE = 4,
		FMUSIC_XM_SETFINETUNE = 5,
		FMUSIC_XM_PATTERNLOOP = 6,
		FMUSIC_XM_SETTREMOLOWAVE = 7,
		FMUSIC_XM_SETPANPOSITION16 = 8,
		FMUSIC_XM_RETRIG = 9,
		FMUSIC_XM_NOTECUT = 12,
		FMUSIC_XM_NOTEDELAY = 13,
		FMUSIC_XM_PATTERNDELAY = 14
	};


	public class uF_XM
	{
		public string idtext;//[17];
		public string modulename;//[20];
		public byte oneA;
		public string trackername;//[20];
		public ushort version;
	}

	public class uF_PATTERN_HEADER
	{
		//public union
		//{
		//	public uint loadxm_pat;
		public	uint headersize;
		//}
		public byte packingtype;
		public ushort rowcount;
		public ushort loadxm_pat_size;
	}


	public class uF_INSTRUMENT_HEADER
	{
		public uint headersize;
		public string instrumentname;//[22];
		public byte instrumenttype;
		public ushort samplecount;
		public uint sampleheadersize;
	}


	public class uF_SAMPLE_HEADER
	{
		public uint loadxm_sample_2;//loadxm_samplelen
		public uint loadxm_s0loopstart;
		public uint loadxm_s0looplen;
		public byte volume;
		public byte finetune;
		public byte []loadxm_s0bytes = new byte[4];//type, pan, relativenoteno, reserved
		public string samplename;
		public byte loadxm_s0loopmode;
		//public union
		//{
		//  unsigned char loadxm_s0loopmode;
		//	char samplename[22];
		//}
	}



	public delegate void uFMOD_open_type(byte[] esi_name);
	public delegate void uFMOD_fread_type(byte [] eax_buffer, uint eds_size);
	public delegate void uFMOD_fclose_type();

	public class uF_MOD_STATE
	{
		private const int FSOUND_Block = 10;
		private const int FSOUND_BlockSize = (1 << FSOUND_Block);
		private const int fragmentsmask = 0xF;
		private const int totalblocks = (fragmentsmask + 1);
		private const int FSOUND_BufferSize = ((FSOUND_BlockSize) * totalblocks);

		public uF_MOD module;

		//public uint[] mmt = new uint[3];//todo, remove me
		//public HANDLE hHeap;
		//public HANDLE hThread;
		public IAudioPlayer hThread;
		//public HWAVEOUT hWaveOut;
		public uint uFMOD_FillBlk;
		//public HANDLE SW_Exit;// file ptr
		public Stream SW_Exit;
		// mix buffer memory block (align 16!)
		public uint[] MixBuf = new uint [FSOUND_BlockSize * 2];
		public byte ufmod_noloop; // enable/disable restart loop
		public byte ufmod_pause; // pause/resume
		public byte[] mix_endflag=new byte[2];
		public uint mmf_module_size; // module size
		public uint mmf_module_posn; // module position
									 //public union
									 //{
									 //	HGLOBAL mmf_module_resource; // resource handle
									 //	char* mmf_module_address;    // module address
									 //}
		 public byte[] mmf_module_address;// module address
		//public HANDLE mmf_module_mapping; //file mapping
		//public uint mmf_pad;//todo, remove me
		public uint ufmod_vol; // global volume scale
		public uFMOD_open_type uFMOD_fopen;
		public uFMOD_fread_type uFMOD_fread;
		public uFMOD_fclose_type uFMOD_fclose;
		public uint unused; //unused (align 16)
		public ushort[] databuf = new ushort[FSOUND_BufferSize * 2];
		//public uF_WMMBlock MixBlock;
		public byte[] RealBlock=new byte[4];
		public uint time_ms;
		public uF_STATS[] tInfo =new uF_STATS [totalblocks];
		public string szTtl;//[24];// track title
		public uF_SAMP DummySamp = new uF_SAMP();

		//added by JS
		//public HINSTANCE hInstance;

		public uF_MOD_STATE()
		{
			for (int i = 0; i < tInfo.Length; i++)
				tInfo[i] = new uF_STATS();
		}

		public int ramp_leftspeed;
		public int ramp_rightspeed;
		public uint mmf_length;
		public uint mmf_mixcount;

		public class _S2_C13
		{
			public uint donote_oldfreq;
			public uint donote_oldpan;
			public int donote_currtick;
		}

		public class _S1
		{
			public uF_SAMP donote_sptr;
			public uF_INST donote_iptr;
			public byte donote_jumpflag;
			public readonly _S2_C13 S2_C13 = new _S2_C13();
		}

		public class _S3
		{
			public uF_NOTE doeff_current;
			public uF_SAMP sample;
		}

		public readonly _S1 S1 = new _S1();
		public readonly _S3 S3 = new _S3();
	}

}